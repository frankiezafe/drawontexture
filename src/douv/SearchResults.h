/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   HitResult.h
 * Author: frankiezafe
 *
 * Created on December 2, 2018, 4:26 PM
 */

#ifndef DOUV_HITRESULT_H
#define DOUV_HITRESULT_H

#include "Face.h"

#define INTERSECTION_FACE_NUM 4

namespace douv {

    template<typename W, typename U>
    struct IntersectionFace {
        const Face<W, U>* face;
        bool is_inside;
        bool wraps;
        uint8_t verts_inside;
        uint8_t verts_wrapped;
        uint8_t intersection_num;
        U* intersections;

        IntersectionFace() :
        face(0),
        is_inside(false), wraps(false),
        verts_inside(0), verts_wrapped(0),
        intersection_num(0), intersections(0) {
            intersections = new U[INTERSECTION_FACE_NUM];
        }

        IntersectionFace(const IntersectionFace& src) :
        face(0),
        is_inside(false), wraps(false),
        intersection_num(0), intersections(0) {
            intersections = new U[INTERSECTION_FACE_NUM];
            for (uint8_t i = 0; i < INTERSECTION_FACE_NUM; ++i) {
                intersections[i] = src.intersections[i];
            }
            face = src.face;
            is_inside = src.is_inside;
            wraps = src.wraps;
            verts_inside = src.verts_inside;
            verts_wrapped = src.verts_wrapped;
            intersection_num = src.intersection_num;
        }

        ~IntersectionFace() {
            delete[] intersections;
        }

    };

    template<typename W, typename U>
    struct IntersectionResults {
        const Face<W, U>* face;
        std::vector< IntersectionFace<W, U> > ifaces;
        bool success;

        IntersectionResults<W, U>() : success(false) {
        }

        void reset() {
            if (!ifaces.empty()) {
                ifaces.clear();
            }
            success = false;
        }

    };

    template<typename W, typename U>
    struct InsideFace {
        const Face<W, U>* face;
        HalfEdge<W, U>* half_edge;
        const Edge<W, U>* edge;

        InsideFace() : face(0), half_edge(0), edge(0) {
        }

        InsideFace(const InsideFace& src) {
            face = src.face;
            half_edge = src.half_edge;
            edge = src.edge;
        }
    };

    template<typename W, typename U>
    class InsideResults {
    public:

        U uv_position;
        bool success;
        uint32_t tests;
        std::vector< InsideFace<W, U> > faces;

        InsideResults<W, U>() : success(false), tests(0) {
        }

        void reset() {
            if (!faces.empty()) {
                faces.clear();
            }
            tests = 0;
            success = false;
        }

    };

};

#endif /* DOUV_HITRESULT_H */

