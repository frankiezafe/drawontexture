/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Mesh.h
 * Author: frankiezafe
 *
 * Created on December 2, 2018, 2:52 PM
 */

#ifndef DOUV_MESH_H
#define DOUV_MESH_H

#include "Face.h"
#include "FaceGroup.h"
#include "SearchResults.h"

namespace douv {

    template<typename W, typename U>
    class Mesh {
    public:

        const AABB<W>& xyz_boundingbox;
        const AABB<U>& uv_boundingbox;

        const size_t& face_num;
        const size_t& edge_num;
        const size_t& group_num;

        Mesh<W, U>() :
        xyz_boundingbox(_xyz_boundingbox),
        uv_boundingbox(_uv_boundingbox),
        face_num(_face_num), edge_num(_edge_num), group_num(_group_num),
        _face_num(0), _edge_num(0), _group_num(0) {
            _xyz_boundingbox.dimensions(3);
            reset(true);
        }

        ~Mesh() {

            reset(false);

        }

        void reset(bool for_reuse = true) {


            face_iterator f = faces.begin();
            face_iterator fe = faces.end();
            for (; f != fe; ++f) {
                delete (*f);
            }

            edge_iterator e = edges.begin();
            edge_iterator ee = edges.end();
            for (; e != ee; ++e) {
                delete (*e);
            }

            group_iterator g = groups.begin();
            group_iterator ge = groups.end();
            for (; g != ge; ++g) {
                delete (*g);
            }

            if (for_reuse) {

                _xyz_boundingbox.reset();
                _uv_boundingbox.reset();
                faces.clear();
                edges.clear();
                groups.clear();

            }

        }

        void allocate(size_t s) {

            faces.reserve(s);

        }

        void intersect_uv(IntersectionResults<W, U>& ir) {

            ir.reset();

            if (!ir.face) {
                return;
            }

            uint8_t res;

            for (uint8_t vid = 0; vid < DFACE_NUM; ++vid) {
                if (_uv_boundingbox.inside((*ir.face)[vid].uv)) {
                    res++;
                }
            }
            if (res == 0) {
                if (!ir.face->intersect_aabb_uv(_uv_boundingbox)) {
                    return;
                }
            }

            if (!groups.empty()) {

                group_iterator g = groups.begin();
                group_iterator ge = groups.end();
                for (; g != ge; ++g) {
                    (*g)->intersect_uv(ir);
                }

            }

        }

        void inside_uv(InsideResults<W, U>& ir) {

            ir.reset();

            if (_uv_boundingbox.inside(ir.uv_position)) {

                if (!groups.empty()) {

                    group_iterator g = groups.begin();
                    group_iterator ge = groups.end();
                    for (; g != ge; ++g) {
                        (*g)->inside_uv(ir);
                    }

                }

            } else {

                ir.success = false;

            }

            ir.tests++;

        }

        void push(
                const Vertex<W, U>& v0,
                const Vertex<W, U>& v1,
                const Vertex<W, U>& v2) {

            _xyz_boundingbox.push(v0.xyz);
            _xyz_boundingbox.push(v1.xyz);
            _xyz_boundingbox.push(v2.xyz);

            _uv_boundingbox.push(v0.uv);
            _uv_boundingbox.push(v1.uv);
            _uv_boundingbox.push(v2.uv);

            Face<W, U>* f = new Face<W, U>();
            f->set(v0, v1, v2);
            faces.push_back(f);

        }

        Edge<W, U>* seek_edge(const Vertex<W, U>* v0, const Vertex<W, U>* v1) {

            edge_iterator e = edges.begin();
            edge_iterator ee = edges.end();
            for (; e != ee; ++e) {
                if ((*e)->match(v0, v1)) {
                    return (*e);
                }
            }

            return 0;

        }

        void finalise(bool generate_groups = true) {

            bool* grouped = new bool[ faces.size() ];
            size_t grouped_id = 0;

            face_iterator f;
            face_iterator fe;
            f = faces.begin();
            fe = faces.end();
            for (; f != fe; ++f, ++grouped_id) {

                for (uint8_t i = 0; i < DFACE_NUM; ++i) {

                    HalfEdge<W, U>* he = (*f)->half_edge(i);
                    Edge<W, U>* e = seek_edge(he->verts.first, he->verts.second);
                    if (!e) {
                        e = new Edge<W, U>();
                        edges.push_back(e);
                    }
                    e->push(he);

                }

                grouped[ grouped_id ] = false;

            }

            _face_num = faces.size();
            _edge_num = edges.size();

            if (!generate_groups) {

                // all faces inside a big group!
                FaceGroup<W, U>* g = new FaceGroup<W, U>();
                groups.push_back(g);

                f = faces.begin();
                for (; f != fe; ++f, ++grouped_id) {
                    g->push(*f, true);
                }
                return;

            }

            size_t used_faces = 0;
            while (used_faces != faces.size()) {

                grouped_id = 0;
                FaceGroup<W, U>* g = new FaceGroup<W, U>();
                groups.push_back(g);

                f = faces.begin();
                for (; f != fe; ++f, ++grouped_id) {
                    if (grouped[ grouped_id ]) {
                        continue;
                    }
                    if (g->push(*f)) {
                        grouped[ grouped_id ] = true;
                        used_faces++;
                    }
                }

            }

            delete[] grouped;

            _group_num = groups.size();

        }

        inline const Face<W, U>* operator[](size_t i) const {
            return faces[i];
        }

        const FaceGroup<W, U>& group(size_t i) const {
            return *groups[i];
        }

        void get_xyz(std::vector< float >& data) {

            data.reserve(faces.size() * DFACE_NUM * 3);

            face_iterator it = faces.begin();
            face_iterator ite = faces.end();
            for (; it != ite; ++it) {
                for (uint8_t i = 0; i < DFACE_NUM; ++i) {
                    data.push_back((*it)->vert(i)->xyz.x);
                    data.push_back((*it)->vert(i)->xyz.y);
                    data.push_back((*it)->vert(i)->xyz.z);
                }
            }

        }

        void get_uv(std::vector< float >& data) {

            data.reserve(faces.size() * DFACE_NUM * 2);

            face_iterator it = faces.begin();
            face_iterator ite = faces.end();
            for (; it != ite; ++it) {
                for (uint8_t i = 0; i < DFACE_NUM; ++i) {
                    data.push_back((*it)->vert(i)->uv.x);
                    data.push_back((*it)->vert(i)->uv.y);
                }
            }

        }

        void get_normals(std::vector< float >& data, float size) {

            data.reserve(faces.size() * DFACE_NUM * 4);

            face_iterator it = faces.begin();
            face_iterator ite = faces.end();
            for (; it != ite; ++it) {
                for (uint8_t i = 0; i < DFACE_NUM; ++i) {

                    const Vertex<W, U>* v = (*it)->vert(i);
                    const HalfEdge<W, U>* e = (*it)->half_edge(i);

                    data.push_back(v->uv.x + e->uv_dir.x * e->uv_length * 0.5);
                    data.push_back(v->uv.y + e->uv_dir.y * e->uv_length * 0.5);
                    data.push_back(v->uv.x + e->uv_dir.x * e->uv_length * 0.5 + e->uv_normal.x * size);
                    data.push_back(v->uv.y + e->uv_dir.y * e->uv_length * 0.5 + e->uv_normal.y * size);

                }
            }

        }

    protected:

        std::vector< Face<W, U>* > faces;
        std::vector< Edge<W, U>* > edges;
        std::vector< FaceGroup<W, U>* > groups;

        size_t _face_num;
        size_t _edge_num;
        size_t _group_num;

        AABB<W> _xyz_boundingbox;
        AABB<U> _uv_boundingbox;

        typedef typename vector< Face<W, U>* >::iterator face_iterator;
        typedef typename vector< Edge<W, U>* >::iterator edge_iterator;
        typedef typename vector< FaceGroup<W, U>* >::iterator group_iterator;

    };

};

#endif /* DOUV_MESH_H */

