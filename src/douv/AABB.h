/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AABB.h
 * Author: frankiezafe
 *
 * Created on December 2, 2018, 12:18 PM
 */

#ifndef DOUV_AABB_H
#define DOUV_AABB_H

namespace douv {

    template <typename U>
    class AABB {
    public:

        const U& min;
        const U& max;
        const U& size;
        const U& center;
        const bool& initialised;
        const uint8_t& dim;

        AABB<U>() :
        min(_min), max(_max), size(_size), center(_center),
        initialised(_initialised), dim(_dim),
        _corners(0), _initialised(false), _dim(2) {
            _corners = new U[4];
        }

        AABB(const AABB<U>& orig) :
        initialised(_initialised), _initialised(false) {

            _corners = new U[4];

            _min = orig.min;
            _max = orig.max;
            _size = orig.size;
            _center = orig.center;
            _initialised = orig.initialised;
            _dim = orig.dim;

            for (uint8_t i = 0; i < 4; ++i) {
                _corners[i] = orig.corners[i];
            }

        }

        ~AABB() {

            delete[] _corners;

        }

        void dimensions(const uint8_t& i) {
            _dim = i;
        }

        void reset() {

            for (uint8_t i = 0; i < dim; ++i) {
                _zero[i] = 0;
            }

            _min = _zero;
            _max = _zero;
            _size = _zero;
            _center = _zero;

            for (uint8_t i = 0; i < 4; ++i) {
                _corners[i] = _zero;
            }

            _initialised = false;

        }

        void push(const U& v) {

            if (!_initialised) {

                _min = v;
                _max = v;
                _size = _zero;
                _center = v;
                _initialised = true;

                for (uint8_t i = 0; i < 4; ++i) {
                    _corners[i] = v;
                }

            } else {

                for (uint8_t i = 0; i < dim; ++i) {
                    if (_min[i] > v[i]) {
                        _min[i] = v[i];
                    }
                    if (_max[i] < v[i]) {
                        _max[i] = v[i];
                    }
                }

                _size = _max - _min;
                _center = _min + _size * 0.5;

                _corners[0] = _min;
                _corners[1] = U(_max.x, _min.y);
                _corners[2] = _max;
                _corners[3] = U(_min.x, _max.y);

            }

        }

        inline void operator=(const AABB<U>& src) {

            _min = src._min;
            _max = src._max;
            _size = src._size;
            _center = src._center;

        }

        inline bool operator==(const AABB<U>& src) const {

            bool _min_equal = true;
            bool _max_equal = true;
            for (uint8_t i = 0; i < dim; ++i) {
                if (_min[i] != src._min[i]) {
                    _min_equal = false;
                }
                if (_max[i] != src._max[i]) {
                    _max_equal = false;
                }
            }
            return _min_equal && _max_equal;

        }

        inline bool inside(const U& v) const {

            for (uint8_t i = 0; i < dim; ++i) {
                if (v[i] < _min[i] || v[i] > _max[i]) {
                    return false;
                }
            }
            return true;

        }

        inline bool inside(const U& v, const float& tolerance) const {

            for (uint8_t i = 0; i < dim; ++i) {
                if (v[i] + tolerance < _min[i] || v[i] - tolerance > _max[i]) {
                    return false;
                }
            }
            return true;

        }

        inline float biggest() const {

            float out = _size[0];
            for (uint8_t i = 1; i < dim; ++i) {
                if (out < _size[i]) {
                    out = _size[i];
                }
            }
            return out;

        }
        
        inline const U& operator[] ( uint8_t i ) const {
            return _corners[i];
        }

    protected:

        U _min;
        U _max;
        U _size;
        U _center;
        U* _corners;
        bool _initialised;

        uint8_t _dim;
        U _zero;

    };

};

#endif /* DOUV_AABB_H */

