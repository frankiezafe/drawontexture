/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   HalfEdge.h
 * Author: frankiezafe
 *
 * Created on December 2, 2018, 10:56 PM
 */

#ifndef DOUV_HALFEDGE_H
#define DOUV_HALFEDGE_H

#include "Vertex.h"

namespace douv {

    template<typename W, typename U>
    class Edge;

    template<typename W, typename U>
    class Face;

    template<typename W, typename U>
    struct HalfEdge {
        std::pair<
        const Vertex<W, U>*,
        const Vertex<W, U>* > verts;

        float uv_length;
        float uv_length_sq;
        float uv_theta;
        U uv_normal;
        U uv_dir;

        Edge<W, U>* edge;
        Face<W, U>* face;

        HalfEdge<W, U>() :
        verts(0, 0), uv_length(0), uv_length_sq(0), uv_theta(0),
        edge(0), face(0) {
        }

        void set(
                Edge<W, U>* edge,
                Face<W, U>* face,
                const Vertex<W, U>* v0,
                const Vertex<W, U>* v1
                ) {

            this->edge = edge;
            this->face = face;

            verts.first = v0;
            verts.second = v1;
            
            refresh();

        }
        
        void refresh() {
            
            uv_dir = verts.second->uv - verts.first->uv;
            uv_length_sq = uv_dir.x * uv_dir.x + uv_dir.y * uv_dir.y;
            uv_length = sqrt(uv_length_sq);
            uv_dir.x /= uv_length;
            uv_dir.y /= uv_length;
            uv_normal = U(uv_dir.y, -uv_dir.x);
            uv_theta = atan2(uv_dir.y, uv_dir.x);
            
        }

    };

};

#endif /* DOUV_HALFEDGE_H */

