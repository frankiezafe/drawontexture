/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FaceGroup.h
 * Author: frankiezafe
 *
 * Created on December 4, 2018, 11:23 AM
 */

#ifndef DOUV_FACEGROUP_H
#define DOUV_FACEGROUP_H

#include "AABB.h"
#include "Face.h"
#include "SearchResults.h"

namespace douv {

    template<typename W, typename U>
    class FaceGroup {
    public:

        const AABB<U>& uv_boundingbox;

        FaceGroup() : uv_boundingbox(_uv_boundingbox) {
        }

        bool push(const Face<W, U>* f, bool force = false) {

            bool oktoadd = force;

            if (!force) {

                if (faces.empty()) {

                    oktoadd = true;

                } else {

                    for (uint8_t i = 0; i < DFACE_NUM; ++i) {
                        if (uv_boundingbox.inside(f->vert(i)->uv, AABB_INSIDE_TOLERANCE)) {
                            oktoadd = true;
                            break;
                        }
                    }

                    if (oktoadd) {
                        oktoadd = false;
                        for (uint8_t i = 0; i < DFACE_NUM; ++i) {
                            if (connected(f->vert(i))) {
                                oktoadd = true;
                                break;
                            }
                        }
                    }

                }

            }

            if (oktoadd) {

                for (uint8_t i = 0; i < DFACE_NUM; ++i) {
                    _uv_boundingbox.push(f->vert(i)->uv);
                }
                faces.push_back(f);

            }

            return oktoadd;

        }

        void intersect_uv(IntersectionResults<W, U>& ir) {

            uint8_t res_inside;
            uint8_t res_wrapped;
            
            for (uint8_t vid = 0; vid < DFACE_NUM; ++vid) {
                if (_uv_boundingbox.inside((*ir.face)[vid].uv)) {
                    res_wrapped++;
                }
            }
            face_iterator it;
            face_iterator ite = faces.end();
            if (res_wrapped == 0) {
                if (ir.face->wrap_uv(_uv_boundingbox)) {
                    // adding all faces!
                    it = faces.begin();
                    for (; it != ite; ++it) {
                        IntersectionFace<W, U> infa;
                        infa.face = (*it);
                        infa.is_inside = true;
                        ir.success = true;
                        ir.ifaces.push_back(infa);
                    }
                    return;
                }
                if (!ir.face->intersect_aabb_uv(_uv_boundingbox)) {
                    return;
                }
            }

            it = faces.begin();
            for (; it != ite; ++it) {

                const Face<W, U>* f = (*it);

                // given face is inside mesh face?
                res_inside = 0;
                for (uint8_t vid = 0; vid < DFACE_NUM; ++vid) {
                    if (f->inside_uv((*ir.face)[vid].uv)) {
                        res_inside++;
                    }
                }
                if (res_inside == DFACE_NUM) {
                    IntersectionFace<W, U> infa;
                    infa.face = f;
                    infa.wraps = true;
                    ir.success = true;
                    ir.ifaces.push_back(infa);
                    continue;
                }

                // given face warps mesh faces?
                res_wrapped = 0;
                for (uint8_t vid = 0; vid < DFACE_NUM; ++vid) {
                    if (ir.face->inside_uv((*f)[vid].uv)) {
                        res_wrapped++;
                    }
                }
                if (res_wrapped == DFACE_NUM) {
                    IntersectionFace<W, U> infa;
                    infa.face = f;
                    infa.is_inside = true;
                    ir.success = true;
                    ir.ifaces.push_back(infa);
                    continue;
                }

                // given face intersect mesh faces
                for (uint8_t vid = 0; vid < DFACE_NUM; ++vid) {
                    IntersectionFace<W, U> infa;
                    if (f->intersect_triangle_uv(
                            ir.face,
                            infa.intersections,
                            infa.intersection_num)) {
                        infa.face = f;
                        infa.verts_inside = res_inside;
                        infa.verts_wrapped = res_wrapped;
                        ir.success = true;
                        ir.ifaces.push_back(infa);
                    }
                }

            }

        }

        void inside_uv(InsideResults<W, U>& ir) {

            if (uv_boundingbox.inside(ir.uv_position)) {

                face_iterator it = faces.begin();
                face_iterator ite = faces.end();
                for (; it != ite; ++it) {

                    const Face<W, U>* f = (*it);
                    HalfEdge<W, U>* he = f->inside_uv(ir.uv_position);
                    ir.tests++;

                    if (he) {

                        InsideFace<W, U> isf;
                        isf.face = f;
                        isf.half_edge = he;
                        ir.faces.push_back(isf);
                        ir.success = true;

                    } else {

                        if (f->uv_boundingbox.inside(ir.uv_position)) {
                            InsideFace<W, U> isf;
                            isf.face = f;
                            ir.faces.push_back(isf);
                            ir.success = true;
                        }
                        ir.tests++;

                    }

                }

            }

        }

        typedef typename vector< const Face<W, U>* >::iterator face_iterator;

    protected:

        AABB<U> _uv_boundingbox;
        std::vector< const Face<W, U>* > faces;

        bool connected(const Vertex<W, U>* v) {

            face_iterator f = faces.begin();
            face_iterator fe = faces.end();
            for (; f != fe; ++f) {
                for (uint8_t i = 0; i < DFACE_NUM; ++i) {
                    if ((*f)->vert(i)->xyz == v->xyz) {
                        return true;
                    }
                }
            }

            return false;

        }

    };

};

#endif /* DOUV_FACEGROUP_H */
