/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   constant.h
 * Author: frankiezafe
 *
 * Created on December 5, 2018, 7:10 PM
 */

#ifndef DOUV_CONSTANT_H
#define DOUV_CONSTANT_H

#define AABB_INSIDE_TOLERANCE 1e-5
#define EDGE_UV_MIN_DIFFERENCE 1e-4

#endif /* DOUV_CONSTANT_H */

