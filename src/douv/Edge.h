/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Edge.h
 * Author: frankiezafe
 *
 * Created on December 2, 2018, 12:33 PM
 */

#ifndef DOUV_EDGE_H
#define DOUV_EDGE_H

#include <math.h>
#include "HalfEdge.h"

namespace douv {

    template<typename W, typename U>
    class Edge {
    public:

        const bool& inverted;
        const bool& connected;

        Edge<W, U>() :
        inverted(_inverted), connected(_connected),
        _part0(0), _part1(0),
        _inverted(false), _connected(false),
        _theta_01(0), _theta_cos_01(1), _theta_sin_01(0),
        _theta_10(0), _theta_cos_10(1), _theta_sin_10(0) {
        }

        ~Edge() {
        }

        HalfEdge<W, U>* other(HalfEdge<W, U>* he) {

            if (!_part0 || !_part1) {
                return 0;
            }

            if (he == _part1) {
                return _part0;
            } else {
                return _part1;
            }

        }

        bool match(const Vertex<W, U>* v0, const Vertex<W, U>* v1) {

            if (!_part0 || _part1) {
                return false;
            }
            if (
                    (
                    _part0->verts.first->xyz == v0->xyz &&
                    _part0->verts.second->xyz == v1->xyz
                    ) ||
                    (
                    _part0->verts.first->xyz == v1->xyz &&
                    _part0->verts.second->xyz == v0->xyz
                    )
                    ) {
                return true;
            }
            return false;

        }

        void push(HalfEdge<W, U>* he) {

            if (!_part0) {
                _part0 = he;
                _part0->edge = this;
            } else if (!_part1) {
                _part1 = he;
                _part1->edge = this;
                process();
            } else {
                std::cout <<
		            "Edge is already having 2 half edges!" <<
		            std::endl;
            }

        }

        bool uv_project_position(U& pos, HalfEdge<W, U>* from, bool force = false) {

            if (
                    (_connected && !force) ||
                    (from != _part0 && from != _part1)
                    ) {
                return false;
            }

            // getting the opposite edge
            HalfEdge<W, U>* to = other(from);
            // position vector normalised relatively to edge length
            U rel = (pos - from->verts.first->uv) / from->uv_length;
            // getting the percentage of the position on direction
            float d = rel.dot(from->uv_dir);
            // rendering perpendicular vector from position to projected point
            // this vector is parallel therefore parallel to normal
            rel = from->verts.first->uv + from->uv_dir * d * from->uv_length - pos;
            // getting the relative size of this vector compared to edge's length
            float lratio = sqrt(rel.x * rel.x + rel.y * rel.y) / from->uv_length;
            // inverting the percentage if the position is inside the triangle
            if (from->uv_normal.dot(rel) < 0) {
                lratio *= -1;
            }
            // and let the magic happens
            if (_inverted) {
                pos = to->verts.second->uv + to->uv_dir * -d * to->uv_length + to->uv_normal * lratio * to->uv_length;
            } else {
                pos = to->verts.first->uv + to->uv_dir * d * to->uv_length + to->uv_normal * lratio * to->uv_length;
            }

            // confirm that there was a projection
            return true;

        }

        bool uv_project_direction(U& dir, HalfEdge<W, U>* from, bool force = false) {

            if (
                    (_connected && !force) ||
                    (from != _part0 && from != _part1)
                    ) {
                return false;
            }

            //x = x * cs - y * sn;
            //y = x * sn + y * cs;
            if (from == _part0) {
                dir = U(dir.x * _theta_cos_01 - dir.y * _theta_sin_01, dir.x * _theta_sin_01 + dir.y * _theta_cos_01);
            } else {
                dir = U(dir.x * _theta_cos_10 - dir.y * _theta_sin_10, dir.x * _theta_sin_10 + dir.y * _theta_cos_10);
            }

            // confirm that there was a projection
            return true;

        }

        float uv_ratio(HalfEdge<W, U>* from, bool force = false) {

            if (
                    (_connected && !force) ||
                    (from != _part0 && from != _part1)
                    ) {
                return 1;
            }

            if (from == _part0) {
                return _ratio_01;
            }
            return _ratio_10;

        }

    protected:

        HalfEdge<W, U>* _part0;
        HalfEdge<W, U>* _part1;
        bool _inverted;
        bool _connected;

        float _theta_01;
        float _theta_cos_01;
        float _theta_sin_01;

        float _theta_10;
        float _theta_cos_10;
        float _theta_sin_10;

        float _ratio_01;
        float _ratio_10;

        void process() {

            _inverted = _part0->verts.first->xyz != _part1->verts.first->xyz;

            U diff_0;
            U diff_1;
            if (_inverted) {
                diff_0 = _part0->verts.first->uv - _part1->verts.second->uv;
                diff_1 = _part0->verts.second->uv - _part1->verts.first->uv;
            } else {
                diff_0 = _part0->verts.first->uv - _part1->verts.first->uv;
                diff_1 = _part0->verts.second->uv - _part1->verts.second->uv;
            }

            if (
                    (diff_0.x * diff_0.x + diff_0.y * diff_0.y) == 0 &&
                    (diff_1.x * diff_1.x + diff_1.y * diff_1.y) == 0
                    ) {
                _connected = true;
                return;
            }

            _theta_01 = _part1->uv_theta - _part0->uv_theta + M_PI;
            _theta_cos_01 = cos(_theta_01);
            _theta_sin_01 = sin(_theta_01);

            _theta_10 = _part0->uv_theta - _part1->uv_theta + M_PI;
            _theta_cos_10 = cos(_theta_10);
            _theta_sin_10 = sin(_theta_10);

            _ratio_01 = _part1->uv_length / _part0->uv_length;
            _ratio_10 = _part0->uv_length / _part1->uv_length;


        }

    };

};

#endif /* DOUV_EDGE_H */

