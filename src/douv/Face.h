/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Face.h
 * Author: frankiezafe
 *
 * Created on December 2, 2018, 1:47 PM
 */

#ifndef DOUV_FACE_H
#define DOUV_FACE_H

#define DFACE_NUM 3

#include "AABB.h"
#include "Edge.h"
#include "SearchResults.h"

namespace douv {

    template<typename W, typename U>
    class Face {
    public:

        const AABB<U>& uv_boundingbox;
        const W& xyz_center;
        const U& uv_center;

        Face<W, U>() :
        uv_boundingbox(_uv_boundingbox),
        xyz_center(_xyz_center),
        uv_center(_uv_center) {

            _verts = new Vertex<W, U>[DFACE_NUM];
            _hedges = new HalfEdge<W, U>[DFACE_NUM];

        }

        ~Face() {

            delete[] _verts;
            delete[] _hedges;

        }

        void set(
                const Vertex<W, U>& v0,
                const Vertex<W, U>& v1,
                const Vertex<W, U>& v2
                ) {

            _verts[0] = v0;
            _verts[1] = v1;
            _verts[2] = v2;

            _hedges[0].set(0, this, &_verts[0], &_verts[1]);
            _hedges[1].set(0, this, &_verts[1], &_verts[2]);
            _hedges[2].set(0, this, &_verts[2], &_verts[0]);

            refresh();

        }

        void refresh() {

            _xyz_center = W(0, 0, 0);
            _xyz_center += _verts[0].xyz / 3.f;
            _xyz_center += _verts[1].xyz / 3.f;
            _xyz_center += _verts[2].xyz / 3.f;

            _uv_center = U(0, 0);
            _uv_center += _verts[0].uv / 3.f;
            _uv_center += _verts[1].uv / 3.f;
            _uv_center += _verts[2].uv / 3.f;

            for (uint8_t i = 0; i < DFACE_NUM; ++i) {
                _hedges[i].refresh();
            }

            _uv_boundingbox.reset();
            _uv_boundingbox.push(_verts[0].uv);
            _uv_boundingbox.push(_verts[1].uv);
            _uv_boundingbox.push(_verts[2].uv);

        }

        void set_xyz_center(const W& v) {
            _xyz_center = v;
        }

        void set_uv_center(const U& v) {
            _uv_center = v;
        }

        Vertex<W, U>& operator[](uint8_t i) {
            return _verts[i];
        }

        const Vertex<W, U>& operator[](uint8_t i) const {
            return _verts[i];
        }

        const Vertex<W, U>* vert(uint8_t i) const {
            return &_verts[i];
        }

        HalfEdge<W, U>* half_edge(uint8_t i) const {
            return &_hedges[i];
        }

        inline const Vertex<W, U>* verts() const {
            return _verts;
        }

        bool inside_uv_fast(const U& v) const {

            if (!_uv_boundingbox.inside(v)) {
                return false;
            }

            bool over = true;
            for (uint8_t i = 0; i < DFACE_NUM; ++i) {
                U rel(v.x - _verts[i].uv.x, v.y - _verts[i].uv.y);
                if (rel.dot(_hedges[i].uv_normal) > 0) {
                    over = false;
                    break;
                }
            }
            return over;

        }

        HalfEdge<W, U>* inside_uv(const U& v) const {

            if (!inside_uv_fast(v)) {
                return 0;
            }

            uint8_t sel = 0;
            float shortest = 0;

            for (uint8_t i = 0; i < DFACE_NUM; ++i) {

                U rel = (v - _verts[i].uv) / _hedges[i].uv_length;

                float d = rel.dot(_hedges[i].uv_dir);
                rel = _verts[i].uv + _hedges[i].uv_dir * d * _hedges[i].uv_length - v;
                float l_sq = rel.x * rel.x + rel.y * rel.y;

                if (i == 0 || shortest > l_sq) {
                    shortest = l_sq;
                    sel = i;
                }

            }

            return &_hedges[sel];

        }

        bool intersect_uv_fast(const Face<W, U>* other) {

            // any vertices inside me?
            for (uint8_t i = 0; i < DFACE_NUM; ++i) {
                if (inside_uv_fast((*other)[i].uv)) {
                    return true;
                }
            }

            // nope? lets try it the other way
            for (uint8_t i = 0; i < DFACE_NUM; ++i) {
                if (other->inside_uv_fast(_verts[i].uv)) {
                    return true;
                }
            }

            // still no result? let's test edges
            for (uint8_t i = 0, j = 1; i < DFACE_NUM; ++i, ++j) {
                j %= DFACE_NUM;
                for (uint8_t k = 0, l = 1; k < DFACE_NUM; ++k, ++l) {
                    l %= DFACE_NUM;
                    if (intersect_uv_segment(
                            _verts[i].uv, _verts[j].uv,
                            (*other)[k].uv, (*other)[l].uv
                            )) {
                        return true;
                    }
                }
            }


            return false;

        }

        bool intersect_triangle_uv(
                const Face<W, U>* other,
                U* pts,
                uint8_t& pt_num) const {

            bool out = false;
            U res;
            pt_num = 0;

            for (uint8_t i = 0, j = 1; i < DFACE_NUM; ++i, ++j) {
                j %= DFACE_NUM;
                for (uint8_t k = 0, l = 1; k < DFACE_NUM; ++k, ++l) {
                    l %= DFACE_NUM;
                    if (intersect_uv_segment(
                            _verts[i].uv, _verts[j].uv,
                            (*other)[k].uv, (*other)[l].uv,
                            &res
                            )) {
                        pts[pt_num] = res;
                        pt_num++;
                        out = true;
                    }
                }
            }

            return out;

        }

        bool intersect_aabb_uv(const AABB<U>& aabb) const {

            for (uint8_t i = 0, j = 1; i < 4; ++i, ++j) {
                j %= 4;
                for (uint8_t k = 0, l = 1; k < DFACE_NUM; ++k, ++l) {
                    l %= DFACE_NUM;
                    if (intersect_uv_segment(
                            _verts[k].uv, _verts[l].uv,
                            aabb[i], aabb[j]
                            )) {
                        return true;
                    }
                }
            }

            return false;

        }

        bool intersect_uv_segment(
                const U& a0, const U& a1,
                const U& b0, const U& b1,
                U* result = 0) const {
            float a = a1.x - a0.x;
            float b = b0.x - b1.x;
            float c = a1.y - a0.y;
            float d = b0.y - b1.y;
            float e = b0.x - a0.x;
            float f = b0.y - a0.y;
            float denom = a * d - b * c;
            if (abs(denom) < 1e-5) {
                // parrallel
                return false;
            } else {
                float t = (e * d - b * f) / denom;
                float u = (a * f - e * c) / denom;
                if (
                        (t >= 0 && t <= 1) &&
                        (u >= 0 && u <= 1)
                        ) {
                    if (result != 0) {
                        result->x = a0.x + t * (a1.x - a0.x);
                        result->y = a0.y + t * (a1.y - a0.y);
                    }
                    return true;
                }
            }
            return false;
        }
        
        bool wrap_uv(const AABB<U>& aabb) const {
            
            for (uint8_t i = 0; i < 4; ++i) {
                if ( !inside_uv( aabb[i] ) ) {
                    return false;
                }
            }
            return true;
            
        }

    protected:

        Vertex<W, U>* _verts;
        HalfEdge<W, U>* _hedges;

        AABB<U> _uv_boundingbox;
        W _xyz_center;
        U _uv_center;

    };

};

#endif /* DOUV_FACE_H */

