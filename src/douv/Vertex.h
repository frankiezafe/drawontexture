/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Vertex.h
 * Author: frankiezafe
 *
 * Created on December 2, 2018, 12:12 PM
 */

#ifndef DOWV_VERTEX_H
#define DOWV_VERTEX_H

#include <math.h>
#include "constant.h"

namespace douv {

    template <typename W, typename U>
    class Vertex {
    public:

        W xyz;
        U uv;

        Vertex<W, U>() {
        }

        Vertex<W, U>(
                const W& v3d, const U& v2d
                ) {

            xyz = v3d;
            uv = v2d;

        }

        Vertex<W, U>(
                const float& x, const float& y, const float& z,
                const float& u, const float& v
                ) {

            xyz = W(x, y, z);
            uv = U(u, v);

        }

        Vertex<W, U>(
                const Vertex<W, U>& orig
                ) {

            xyz = orig.xyz;
            uv = orig.uv;

        }

        virtual ~Vertex<W, U>() {
        }

        inline void operator=(const Vertex<W, U>& src) {

            xyz = src.xyz;
            uv = src.uv;

        }

        inline bool operator==(const Vertex<W, U>& src) const {

            return xyz == src.xyz && uv == src.uv;

        }

    };

};

#endif /* DOWV_VERTEX_H */

