/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UVTravellers.h
 * Author: frankiezafe
 *
 * Created on December 5, 2018, 12:38 PM
 */

#ifndef UVTRAVELLERS_H
#define UVTRAVELLERS_H

#include "ofMain.h"
#include "douv/Mesh.h"

struct Traveler {
    
    const douv::Face<ofVec3f, ofVec2f>* face;
	douv::HalfEdge<ofVec3f, ofVec2f>* half_edge;
    ofVec2f position;
    ofVec2f direction;
    ofVec2f gravity;
    ofVec4f rgba0;
	ofVec4f rgba1;
    float speed;
    float age;
    float lifetime;
	float deviation;

    Traveler() :
    face(0),
    half_edge(0),
    rgba0(1, 1, 1, 1), 
    rgba1(0, 0, 0, 1), 
	speed(0),
    age(0), lifetime(10),
	deviation(0.01) {
        direction.set(ofRandomf(), ofRandomf());
        direction.normalize();
    }

    Traveler(const Traveler& src) {
        face = src.face;
        position = src.position;
        direction = src.direction;
        rgba0 = src.rgba0;
        speed = src.speed;
        age = src.age;
        lifetime = src.lifetime;
    }

    void operator=(const Traveler& src) {
        face = src.face;
        position = src.position;
        direction = src.direction;
        rgba0 = src.rgba0;
        speed = src.speed;
        age = src.age;
        lifetime = src.lifetime;
    }

    void reset() {
        face = 0;
        age = 0;
        rgba0.set(1, 1, 1, 1);
        direction.set(ofRandomf(), ofRandomf());
        direction.normalize();
    }

};

class UVTravelers {
public:

    UVTravelers() : _mesh(0) {
    }

    ~UVTravelers() {
        std::vector< Traveler* >::iterator it = travs.begin();
        std::vector< Traveler* >::iterator ite = travs.end();
        for (; it != ite; ++it) {
            delete (*it);
        }
    }

    void init(douv::Mesh<ofVec3f, ofVec2f>* m) {
        _mesh = m;
    }
    
    void bam( const ofVec2f& position, size_t num ) {
        
        if (!_mesh) {
            return;
        }

		douv::InsideResults<ofVec3f, ofVec2f> ht;
		ht.uv_position = position;
        _mesh->inside_uv(ht);
		if ( !ht.success ) {
			return;
		}
		

        for ( size_t i = 0; i < num; ++i ) {

			float r = ofRandomuf() * M_PI * 2;
			ofVec2f offset( cos(r), sin(r) );
			offset *= 0.001 * ofRandomuf() * 0.01;

            Traveler* t = new Traveler();
            t->position = position + offset;
            t->speed = 0.01 + ofRandomuf() * 0.03;
			t->rgba0.set( 0.9 + ofRandomuf() * 0.1, 0.5 + ofRandomuf() * 0.3, 0.1 + ofRandomuf() * 0.1, 1 );
			t->rgba1.set( 0.1 + ofRandomuf() * 0.3, 0.9 + ofRandomuf() * 0.1, 0.7 + ofRandomuf() * 0.3, 1 );
			t->lifetime = 2 + ofRandomuf() * 2;
			t->deviation = 0.01 + ofRandomuf() * 0.1;
			t->direction = offset.normalized();
			t->gravity.set( 0, -0.01 );
            travs.push_back( t );
        }

		std::cout << "travelers: " << travs.size() << std::endl;
        
    }

    void update(float delta_time_sec) {

        if (!_mesh || travs.empty()) {
            return;
        }

        std::vector< Traveler* > alive;
        std::vector< Traveler* > zombies;

        douv::InsideResults<ofVec3f, ofVec2f> ht;
        std::vector< Traveler* >::iterator it = travs.begin();
        std::vector< Traveler* >::iterator ite = travs.end();
        for (; it != ite; ++it) {

            Traveler* t = (*it);
            t->age += delta_time_sec;

            if (t->age < t->lifetime) {

                t->position += t->direction * t->speed * delta_time_sec;

				t->direction += ofVec2f( ofRandomf() * t->deviation, ofRandomf() * t->deviation );
				t->direction += t->gravity;

				t->direction.normalized();

				ht.uv_position = t->position;
                _mesh->inside_uv(ht);

                if (!ht.success && ( !t->face || !t->half_edge) ) {
                    zombies.push_back(t);
                    continue;
                }

				bool face_switch = true;

				if (ht.success) {
        	        // still the same face?
					for (size_t f = 0; f < ht.faces.size(); ++f) {
		                douv::InsideFace<ofVec3f, ofVec2f>& isf = ht.faces[f];
		                const douv::Face<ofVec3f, ofVec2f>* face = isf.face;
						if ( !isf.half_edge ) {
							continue;
						}
						if ( !t->face ) {
							t->face = face;
							t->half_edge = isf.half_edge;
		                    face_switch = false;
							break;
						}
		                if (face == t->face) {
							t->half_edge = isf.half_edge;
		                    face_switch = false;
		                    break;
		                }
		            }
				}

				if (face_switch) {
					douv::Edge<ofVec3f, ofVec2f>* edge = t->half_edge->edge;
					if (edge) {
                        t->speed *= edge->uv_ratio(t->half_edge);
                        edge->uv_project_position(t->position, t->half_edge);
                        edge->uv_project_direction(t->direction, t->half_edge);
                        edge->uv_project_direction(t->gravity, t->half_edge);
						t->half_edge = edge->other( t->half_edge );
						t->face = t->half_edge->face;
                    }
				}

                alive.push_back(t);

            } else {

                zombies.push_back(t);

            }
        }

        travs.swap(alive);

        it = zombies.begin();
        ite = zombies.end();
        for (; it != ite; ++it) {
            delete (*it);
        }

    }
    
    void draw( const ofVec2f& scale ) {
        
		glPointSize( 1.5f );
        glBegin( GL_POINTS );
        
        ofVec2f pos;
        
        std::vector< Traveler* >::iterator it = travs.begin();
        std::vector< Traveler* >::iterator ite = travs.end();
        for (; it != ite; ++it) {
            Traveler* t = (*it);
            pos = t->position * scale;
			float cmult = t->age / t->lifetime;
			cmult *= cmult;
			ofVec4f rgba = t->rgba0 * (1-cmult) + t->rgba1 * cmult;
			if ( cmult > 0.9 ) {
				float darkness = 1 - ( cmult - 0.9 ) * 10;
				//rgba *= ofVec4f( 1,1,1,darkness );
			}
            glColor4f( rgba.x, rgba.y, rgba.z, rgba.w );
            glVertex2f( pos.x, pos.y );
        }
        
        glEnd();
		glPointSize( 1.f );
        
    }

private:

    douv::Mesh<ofVec3f, ofVec2f>* _mesh;
    std::vector< Traveler* > travs;

};


#endif /* UVTRAVELLERS_H */

