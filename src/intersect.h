#ifndef INTERSECT_H
#define INTERSECT_H

#define DATA_FILE "ship-vert-uv.txt"
#define TOLERANCE 7

#include "ofMain.h"
#include "douv/Mesh.h"

class DraggableFace : public douv::Face<ofVec3f, ofVec2f> {
public:
	
	int dragged;
	
	DraggableFace() : douv::Face<ofVec3f, ofVec2f>() {
		dragged = -1;
	}
	
};

class intersect : public ofBaseApp {
public:

	void setup();
	void update();
	void draw();

	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);

private:
	
	douv::Mesh<ofVec3f, ofVec2f> uvmesh;
        douv::IntersectionResults<ofVec3f, ofVec2f> intersect_res;
	std::vector< float > uvmesh_uvs;
        std::string intersect_duration;
        std::vector< int64_t > intersect_avrg;
	
	DraggableFace dface;
	
	ofVec2f view_mouse_init;
	bool view_rotation_enabled;
	ofVec3f view_rotation;
	ofVec3f view_rotation_init;
	
	ofVec2f offset_mouse;

};

#endif /* INTERSECT_H */