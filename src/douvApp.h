/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   douvApp.h
 * Author: frankiezafe
 *
 * Created on December 2, 2018, 3:03 PM
 */

#ifndef DOUVAPP_H
#define DOUVAPP_H

#define TEXTURE_SIZE 1024
//#define DATA_FILE "cylinder-vert-uv.txt"
#define DATA_FILE "ship-vert-uv.txt"

#include "ofMain.h"
#include "douv/Mesh.h"
#include "douv/AABB.h"
#include "douv/SearchResults.h"
#include "UVTravelers.h"
#include "SearchResults.h"

class ofUVMesh : public douv::Mesh<ofVec3f, ofVec2f> {
};

class ofUVHiTest : public douv::InsideResults<ofVec3f, ofVec2f> {
};

class douvApp : public ofBaseApp {
public:

    void setup();
    void update();
    void draw();

    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);

private:

    ofFbo draw_surface;
    ofVec2f surf_size;

    ofVec2f view_mouse_init;
    bool view_rotation_enabled;
    ofVec3f view_rotation;
    ofVec3f view_rotation_init;

    float last_time;

    ofUVMesh uvmesh;
    std::vector< float > uvmesh_xyzs;
    std::vector< float > uvmesh_uvs;
    std::vector< float > uvmesh_normals;

    ofUVHiTest ht;
    ofVec3f ht_center;

    void draw_aabb(const douv::AABB< ofVec2f >&);
    void draw_aabb(const douv::AABB< ofVec3f >&);
    
    UVTravelers travelers;

};

#endif /* DOUVAPP_H */

