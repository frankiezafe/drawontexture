#include "ofApp.h"

//--------------------------------------------------------------
//                          UVVERTEX
//--------------------------------------------------------------

void UVVertex::set(float x, float y, float z, float u, float v) {
    xyz.set(x, y, z);
    uv0.set(u, v);
}

void UVVertex::mult(float m_xyz, float m_uv0) {
    xyz *= m_xyz;
    uv0 *= m_uv0;
}

void UVVertex::opengl() {
    glTexCoord2f(uv0.x, uv0.y);
    glVertex3f(xyz.x, xyz.y, xyz.z);
}

void UVVertex::opengl_uv0() {
    glVertex2f(uv0.x, uv0.y);
}

//--------------------------------------------------------------
//                          UVEDGE
//--------------------------------------------------------------

UVEdge::UVEdge() : _bb0(0), _bb1(0) {
}

UVEdge::UVEdge(const UVEdge& src) {
    
    _a0 = src._a0;
    _b0 = src._b0;
    _bb0 = src._bb0;
    _bb1 = src._bb1;
    _dir0 = src._dir0;
    _dir1 = src._dir1;
    _center = src._center;
    
}

bool UVEdge::compare(const UVVertex& src_a, const UVVertex& src_b) const {
    return
        ( _a0.xyz == src_a.xyz && _b0.xyz == src_b.xyz) ||
        (_a0.xyz == src_b.xyz && _b0.xyz == src_a.xyz);
}

bool UVEdge::push(const UVVertex& src_a, const UVVertex& src_b, UVAABB* _bb) {
    if (_bb0 == 0) {

        _bb0 = _bb;
        _a0 = src_a;
        _b0 = src_b;

    } else if (_bb1 == 0) {

        _bb1 = _bb;
        _a1 = src_a;
        _b1 = src_b;
        build_teleport();

    } else {

        std::cout << "WTF? edge is used more than 2 times???" << std::endl;
        return false;

    }
    return true;
}

void UVEdge::build_teleport() {
    
    _dir0 = _b0.uv0 - _a0.uv0;
    _dir0.normalize();
    _dir1 = _b1.uv0 - _a1.uv0;
    _dir1.normalize();

    ofVec2f mid0 = (_a0.uv0 + (_b0.uv0 - _a0.uv0) * 0.5);
    ofVec2f mid1 = (_a1.uv0 + (_b1.uv0 - _a1.uv0) * 0.5);
    _center = mid0 + (mid1 - mid0) * 0.5;
    
}

void UVEdge::debug_display() {
    
    // cross
    glVertex2f(_center.x - 10, _center.y-1);
    glVertex2f(_center.x + 10, _center.y-1);
    glVertex2f(_center.x-1, _center.y - 10);
    glVertex2f(_center.x-1, _center.y + 10);
    glVertex2f(_center.x - 10, _center.y+1);
    glVertex2f(_center.x + 10, _center.y+1);
    glVertex2f(_center.x+1, _center.y - 10);
    glVertex2f(_center.x+1, _center.y + 10);

    glVertex2f(_center.x, _center.y);
    glVertex2f(_bb0->center.x, _bb0->center.y);
    glVertex2f(_center.x, _center.y);
    glVertex2f(_bb1->center.x, _bb1->center.y);
    
////    glVertex2f(_center.x, _center.y);
////    glVertex2f(_a0.uv0.x, _a0.uv0.y);
////    glVertex2f(_center.x, _center.y);
////    glVertex2f(_b0.uv0.x, _b0.uv0.y);
////
////    glVertex2f(_center.x, _center.y);
////    glVertex2f(_a1.uv0.x, _a1.uv0.y);
////    glVertex2f(_center.x, _center.y);
////    glVertex2f(_b1.uv0.x, _b1.uv0.y);

}

//--------------------------------------------------------------
//                      UVTFACE
//--------------------------------------------------------------

UVFace::UVFace() {
    verts = new UVVertex[4];
}

UVFace::~UVFace() {
    delete [] verts;
}

void UVFace::render_uvbb() {
    
    uvbb.min = verts[0].uv0;
    uvbb.max = verts[0].uv0;
    for (uint i = 1; i < 4; ++i) {
        if (uvbb.min.x > verts[i].uv0.x) {
            uvbb.min.x = verts[i].uv0.x;
        }
        if (uvbb.min.y > verts[i].uv0.y) {
            uvbb.min.y = verts[i].uv0.y;
        }
        if (uvbb.max.x < verts[i].uv0.x) {
            uvbb.max.x = verts[i].uv0.x;
        }
        if (uvbb.max.y < verts[i].uv0.y) {
            uvbb.max.y = verts[i].uv0.y;
        }
    }
    uvbb.size = uvbb.max - uvbb.min;
    uvbb.center = uvbb.min + uvbb.size * 0.5;
    
}

//--------------------------------------------------------------
//                      UVTRAVELLER
//--------------------------------------------------------------

UVTraveller::UVTraveller() :
pos(_pos), previous_pos(_previous_pos), dir(_dir), speed(_speed) {
    init();
}

UVTraveller::UVTraveller(const UVTraveller& src) :
pos(_pos), previous_pos(_previous_pos), dir(_dir), speed(_speed) {
    _pos = src.pos;
    _dir = src.dir;
    _speed = src.speed;
    init();
}

UVTraveller::UVTraveller(
        const float& x, const float& y,
        const float& dx, const float& dy,
        const float& s) :
pos(_pos), previous_pos(_previous_pos), dir(_dir), speed(_speed) {
    _pos.set(x, y);
    _dir.set(dx, dy);
    _speed = s;
    init();
}

UVTraveller::UVTraveller(const ofVec2f& p, const ofVec2f& d, const float& s) :
pos(_pos), previous_pos(_previous_pos), dir(_dir), speed(_speed) {
    _pos = p;
    _dir = d;
    _speed = s;
    init();
}

UVTraveller::~UVTraveller() {
}

void UVTraveller::move(const float& deltatime) {
    _previous_pos = pos;
    _pos += _dir * deltatime * _speed;
    if (_pos.x <= _min.x || _pos.x >= _max.x) {
        _dir.x *= -1;
    }
    if (_pos.y <= _min.y || _pos.y >= _max.y) {
        _dir.y *= -1;
    }
}

void UVTraveller::move_to(const ofVec2f& p) {
    _previous_pos = pos;
    _pos = p;
}

void UVTraveller::limits(
        const float& x0, const float& y0,
        const float& x1, const float& y1) {
    _min.set(x0, y0);
    _max.set(x1, y1);
}

void UVTraveller::limits(const ofVec2f& min, const ofVec2f& max) {
    _min = min;
    _max = max;
}

void UVTraveller::init() {
    _previous_pos = pos;
    _dir.normalize();
}

//--------------------------------------------------------------
//                      OFAPP
//--------------------------------------------------------------

void ofApp::setup() {

    draw_surface.allocate(1024, 1024, GL_RGBA, 8);

    draw_surface.begin();

    ofClear(0, 0, 0, 255);

    glBegin(GL_QUADS);
    glColor4f(1, 1, 0, 1);
    glVertex2f(0, 0);
    glColor4f(0, 1, 0, 1);
    glVertex2f(draw_surface.getWidth(), 0);
    glColor4f(0, 0, 1, 1);
    glVertex2f(draw_surface.getWidth(), draw_surface.getHeight());
    glColor4f(1, 0, 1, 1);
    glVertex2f(0, draw_surface.getHeight());
    glEnd();

    glColor4f(1, 1, 1, 1);
    glLineWidth(4.f);

    faces = new UVFace[FNUM];

    faces[0].name = "top";
    faces[0].verts[0].set(-1, -1, +1, 0.25, 0.25);
    faces[0].verts[1].set(+1, -1, +1, 0.5, 0.25);
    faces[0].verts[2].set(+1, +1, +1, 0.5, 0.5);
    faces[0].verts[3].set(-1, +1, +1, 0.25, 0.5);

    faces[1].name = "back";
    faces[1].verts[0].set(-1, -1, -1, 0.25, 0);
    faces[1].verts[1].set(-1, -1, +1, 0.25, 0.25);
    faces[1].verts[2].set(+1, -1, +1, 0.5, 0.25);
    faces[1].verts[3].set(+1, -1, -1, 0.5, 0);

    faces[2].name = "left";
    faces[2].verts[0].set(-1, -1, -1, 0, 0.25);
    faces[2].verts[1].set(-1, -1, +1, 0.25, 0.25);
    faces[2].verts[2].set(-1, +1, +1, 0.25, 0.5);
    faces[2].verts[3].set(-1, +1, -1, 0, 0.5);

    faces[3].name = "front";
    faces[3].verts[0].set(-1, +1, +1, 0.25, 0.5);
    faces[3].verts[1].set(+1, +1, +1, 0.5, 0.5);
    faces[3].verts[2].set(+1, +1, -1, 0.5, 0.75);
    faces[3].verts[3].set(-1, +1, -1, 0.25, 0.75);

    faces[4].name = "right";
    faces[4].verts[0].set(+1, -1, +1, 0.5, 0.25);
    faces[4].verts[1].set(+1, -1, -1, 0.75, 0.25);
    faces[4].verts[2].set(+1, +1, -1, 0.75, 0.5);
    faces[4].verts[3].set(+1, +1, +1, 0.5, 0.5);

    faces[5].name = "bottom";
    faces[5].verts[0].set(-1, -1, -1, 1, 0.25);
    faces[5].verts[1].set(+1, -1, -1, 0.75, 0.25);
    faces[5].verts[2].set(+1, +1, -1, 0.75, 0.5);
    faces[5].verts[3].set(-1, +1, -1, 1, 0.5);

    for (uint i = 0; i < FNUM; ++i) {

        for (uint j = 0; j < 4; ++j) {
            faces[i].verts[j].mult(1, draw_surface.getWidth());
        }

        faces[i].render_uvbb();

        glBegin(GL_LINES);
        for (uint j = 0, k = 1; j < 4; ++j, ++k) {
            k %= 4;
            faces[i].verts[j].opengl_uv0();
            faces[i].verts[k].opengl_uv0();
        }
        glEnd();

        ofDrawBitmapString(
                faces[i].name,
                faces[i].uvbb.min.x + 10,
                faces[i].uvbb.min.y + 25
                );

    }

    for (uint i = 0; i < 3; ++i) {
        uvtravellers.push_back(UVTraveller(
                ofVec2f(
                draw_surface.getWidth() * ofRandomf(),
                draw_surface.getHeight() * ofRandomf()),
                ofVec2f(ofRandomf(), ofRandomf()),
                30
                ));
    }

    std::vector<UVTraveller>::iterator it = uvtravellers.begin();
    std::vector<UVTraveller>::iterator ite = uvtravellers.end();
    for (; it != ite; ++it) {
        bound_to_face((*it));
    }

    last_time = ofGetElapsedTimef();

    render_edges();

    glColor4f(1, 0, 0, 1);
    glLineWidth(1.f);
    glBegin(GL_LINES);
    std::vector<UVEdge>::iterator ed = uvedges.begin();
    std::vector<UVEdge>::iterator ede = uvedges.end();
    for (; ed != ede; ++ed) {
        UVEdge& e = (*ed);
        if (e.ready()) {
            e.debug_display();
        }
    }
    glEnd();

    draw_surface.end();

}

UVEdge* ofApp::seek_edge(const UVVertex& a, const UVVertex& b) {

    std::vector<UVEdge>::iterator it = uvedges.begin();
    std::vector<UVEdge>::iterator ite = uvedges.end();
    for (; it != ite; ++it) {
        if ((*it).compare(a, b)) {
            return &(*it);
        }
    }
    return 0;

}

void ofApp::render_edges() {

    for (uint i = 0; i < FNUM; ++i) {
        for (uint j = 0, k = 1; j < 4; ++j, ++k) {
            k %= 4;
            const UVVertex& vj = faces[i].verts[j];
            const UVVertex& vk = faces[i].verts[k];
            UVEdge* uve = seek_edge( vj, vk );
            if (!uve) {
                UVEdge e;
                e.push( vj, vk, &faces[i].uvbb);
                uvedges.push_back(e);
            } else {
                uve->push( vj, vk, &faces[i].uvbb);
            }
        }
    }

    std::cout << uvedges.size() << std::endl;

}

void ofApp::bound_to_face(UVTraveller& t) {

    for (uint i = 0; i < FNUM; ++i) {
        if (
                t.pos.x >= faces[i].uvbb.min.x &&
                t.pos.y >= faces[i].uvbb.min.y &&
                t.pos.x <= faces[i].uvbb.max.x &&
                t.pos.y <= faces[i].uvbb.max.y
                ) {
            t.limits(faces[i].uvbb.min, faces[i].uvbb.max);
            return;
        }
    }

    t.move_to(ofVec2f(
            draw_surface.getWidth() * ofRandomf(),
            draw_surface.getHeight() * ofRandomf()));
    bound_to_face(t);

}

void ofApp::exit() {

    delete [] faces;

}

void ofApp::update() {

}

void ofApp::draw() {

    float current_time = ofGetElapsedTimef();
    float delta_time = current_time - last_time;
    last_time = current_time;

    ofSetColor(255, 255, 255, 255);

    ofEnableDepthTest();

    draw_surface.begin();
    std::vector<UVTraveller>::iterator it = uvtravellers.begin();
    std::vector<UVTraveller>::iterator ite = uvtravellers.end();
    for (; it != ite; ++it) {
        (*it).move(delta_time);
        ofDrawLine((*it).previous_pos, (*it).pos);
    }
    draw_surface.end();

    ofDisableDepthTest();

    //    draw_surface.draw(10, 10, 256, 256);
    draw_surface.draw(10, 10);


    ofEnableDepthTest();

    ofPushMatrix();

    ofTranslate(ofGetWidth() * 0.5, ofGetHeight() * 0.5, 0);
    ofRotateX(ofGetElapsedTimeMillis() * 0.01);
    ofRotateY(ofGetElapsedTimeMillis() * 0.005);

    float sc = ofGetHeight() * 0.2;
    glScalef(sc, sc, sc);

    draw_surface.getTexture().bind();

    glBegin(GL_QUADS);
    for (uint i = 0; i < FNUM; ++i) {
        for (uint j = 0; j < 4; ++j) {
            faces[i].verts[j].opengl();
        }
    }
    glEnd();

    draw_surface.getTexture().unbind();

    ofPopMatrix();

}

void ofApp::keyPressed(int key) {

}

void ofApp::keyReleased(int key) {

}

void ofApp::mouseMoved(int x, int y) {

}

void ofApp::mouseDragged(int x, int y, int button) {

}

void ofApp::mousePressed(int x, int y, int button) {

}

void ofApp::mouseReleased(int x, int y, int button) {

}

void ofApp::mouseEntered(int x, int y) {

}

void ofApp::mouseExited(int x, int y) {

}

void ofApp::windowResized(int w, int h) {

}

void ofApp::gotMessage(ofMessage msg) {

}

void ofApp::dragEvent(ofDragInfo dragInfo) {

}