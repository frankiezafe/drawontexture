#include "ofMain.h"
#include "ofApp.h"
#include "douvApp.h"
#include "intersect.h"

//========================================================================
int main( ){
    ofSetupOpenGL(800,800, OF_WINDOW);			// <-------- setup the GL context
//    ofSetupOpenGL(1024,600, OF_FULLSCREEN);			// <-------- setup the GL context
//    ofRunApp( new ofApp());
    ofRunApp( new intersect());
//    ofRunApp( new douvApp());

}