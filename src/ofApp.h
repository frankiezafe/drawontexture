#pragma once

#include "ofMain.h"

#define FNUM 6

class UVVertex {
public:

    ofVec3f xyz;
    ofVec2f uv0;

    void set(float x, float y, float z, float u, float v);

    void mult(float m_xyz, float m_uv0);

    void opengl();

    void opengl_uv0();

    inline void operator=(const UVVertex& src) {
        xyz = src.xyz;
        uv0 = src.uv0;
    }

    inline bool operator==(const UVVertex& src) const {
        return xyz == src.xyz && uv0 == src.uv0;
    }

};

struct UVAABB {
    ofVec2f min;
    ofVec2f max;
    ofVec2f size;
    ofVec2f center;
};

class UVEdge {
public:

    UVEdge();

    UVEdge(const UVEdge& src);

    bool compare(const UVVertex& src_a, const UVVertex& src_b) const;

    bool push( const UVVertex& src_a, const UVVertex& src_b, UVAABB* _bb );
    
    inline bool ready() const {
        return _bb1 != 0;
    }
    
    void debug_display();

private:

    UVVertex _a0;
    UVVertex _b0;
    
    UVVertex _a1;
    UVVertex _b1;
    
    UVAABB* _bb0;
    UVAABB* _bb1;
    
    ofVec2f _dir0;
    ofVec2f _dir1;
    
    ofVec2f _center;

    void build_teleport();

};

class UVFace {
public:

    UVVertex* verts;
    UVAABB uvbb;
    std::string name;

    UVFace();

    ~UVFace();

    void render_uvbb();
};

class UVTraveller {
public:

    const ofVec2f& pos;
    const ofVec2f& previous_pos;
    const ofVec2f& dir;
    const float& speed;

    UVTraveller();

    UVTraveller(const UVTraveller& src);

    UVTraveller(
            const float& x, const float& y,
            const float& dx, const float& dy,
            const float& s);

    UVTraveller(const ofVec2f& p, const ofVec2f& d, const float& s);

    ~UVTraveller();

    void move(const float& deltatime);

    void move_to(const ofVec2f& p);

    void limits(
            const float& x0, const float& y0,
            const float& x1, const float& y1);

    void limits(const ofVec2f& min, const ofVec2f& max);

private:

    ofVec2f _pos;
    ofVec2f _previous_pos;
    ofVec2f _dir;
    ofVec2f _min;
    ofVec2f _max;
    float _speed;

    void init();

};

class ofApp : public ofBaseApp {
public:

    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    void exit();

private:

    ofFbo draw_surface;

    UVFace* faces;
    std::vector<UVEdge> uvedges;
    std::vector<UVTraveller> uvtravellers;

    float last_time;

    UVEdge* seek_edge(const UVVertex& a, const UVVertex& b);

    void render_edges();

    void bound_to_face(UVTraveller& t);

};
