/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   douvApp.cpp
 * Author: frankiezafe
 * 
 * Created on December 2, 2018, 3:03 PM
 */

#include "douvApp.h"

void douvApp::setup() {

	view_rotation_enabled = false;

	ofBuffer buffer = ofBufferFromFile(DATA_FILE);
	if (buffer.size()) {

		std::vector< douv::Vertex<ofVec3f, ofVec2f> > tmp;

		for (ofBuffer::Line it = buffer.getLines().begin(), end = buffer.getLines().end(); it != end; ++it) {

			string line = *it;
			vector<string> coords = ofSplitString(line, ",");
			if (coords.size() == 5) {
				douv::Vertex<ofVec3f, ofVec2f> v(
						atof(coords[0].c_str()),
						atof(coords[1].c_str()),
						atof(coords[2].c_str()),
						atof(coords[3].c_str()),
						atof(coords[4].c_str())
						);
				tmp.push_back(v);
				if (tmp.size() == 3) {
					uvmesh.push(tmp[0], tmp[1], tmp[2]);
					tmp.clear();
				}
			}

		}

	}

	uvmesh.finalise();
	uvmesh.get_xyz(uvmesh_xyzs);
	uvmesh.get_uv(uvmesh_uvs);
	uvmesh.get_normals(uvmesh_normals, 0.01);

	draw_surface.allocate(TEXTURE_SIZE, TEXTURE_SIZE, GL_RGBA, 8);

	ofDisableDepthTest();
	ofEnableAlphaBlending(); 

	int tw = draw_surface.getWidth();
	int th = draw_surface.getHeight();
	ofVec2f ts(tw, th);

	draw_surface.begin();
	ofClear(0, 0, 0, 255);
	ofNoFill();
	glColor4f(0.2,0.2,0.2,1);
	glBegin(GL_TRIANGLES);
	size_t fnum = uvmesh_uvs.size() / 2;
	for (uint i = 0, uvi = 0; i < fnum; ++i, uvi += 2) {
		glVertex2f(uvmesh_uvs[uvi] * tw, uvmesh_uvs[uvi + 1] * th);
	}
	glEnd();
	draw_surface.end();

	surf_size.set(TEXTURE_SIZE, TEXTURE_SIZE);

	travelers.init( &uvmesh );

}

void douvApp::draw_aabb(const douv::AABB< ofVec2f >& aabb) {
	ofVec2f min = aabb.min * surf_size;
	ofVec2f max = aabb.max * surf_size;
	glVertex2f(min.x, min.y);
	glVertex2f(max.x, min.y);
	glVertex2f(max.x, min.y);
	glVertex2f(max.x, max.y);
	glVertex2f(max.x, max.y);
	glVertex2f(min.x, max.y);
	glVertex2f(min.x, max.y);
	glVertex2f(min.x, min.y);
}

void douvApp::draw_aabb(const douv::AABB< ofVec3f >& aabb) {

	const ofVec3f& min = aabb.min;
	const ofVec3f& max = aabb.max;

	glVertex3f(min.x, min.y, min.z);
	glVertex3f(max.x, min.y, min.z);
	glVertex3f(max.x, min.y, min.z);
	glVertex3f(max.x, max.y, min.z);
	glVertex3f(max.x, max.y, min.z);
	glVertex3f(min.x, max.y, min.z);
	glVertex3f(min.x, max.y, min.z);
	glVertex3f(min.x, min.y, min.z);

	glVertex3f(min.x, min.y, max.z);
	glVertex3f(max.x, min.y, max.z);
	glVertex3f(max.x, min.y, max.z);
	glVertex3f(max.x, max.y, max.z);
	glVertex3f(max.x, max.y, max.z);
	glVertex3f(min.x, max.y, max.z);
	glVertex3f(min.x, max.y, max.z);
	glVertex3f(min.x, min.y, max.z);

}

void douvApp::update() {

	float current_time = ofGetElapsedTimef();
	float delta_time = current_time - last_time;
	last_time = current_time;
	
	travelers.update( delta_time );

	ofDisableDepthTest();
	ofEnableAlphaBlending(); 
	//ofDisableAlphaBlending(); 

	int tw = draw_surface.getWidth();
	int th = draw_surface.getHeight();
	ofVec2f ts(tw, th);

	draw_surface.begin();
		
	travelers.draw( surf_size );

	draw_surface.end();
	
	return;

	ofClear(0, 0, 0, 255);

	glBegin(GL_QUADS);
	glColor4f(1, 1, 0, 1);
	glVertex2f(0, 0);
	glColor4f(0, 1, 0, 1);
	glVertex2f(draw_surface.getWidth(), 0);
	glColor4f(0, 0, 1, 1);
	glVertex2f(draw_surface.getWidth(), draw_surface.getHeight());
	glColor4f(1, 0, 1, 1);
	glVertex2f(0, draw_surface.getHeight());
	glEnd();

	ofNoFill();
	glColor4f(1,1,1,1);
	glBegin(GL_TRIANGLES);
	size_t fnum = uvmesh_uvs.size() / 2;
	for (uint i = 0, uvi = 0; i < fnum; ++i, uvi += 2) {
		glVertex2f(uvmesh_uvs[uvi] * tw, uvmesh_uvs[uvi + 1] * th);
	}
	glEnd();

	//	glColor4f(0, 1, 1, 1);
	//	glBegin(GL_LINES);
	//	for (uint i = 0; i < uvmesh.group_size(); ++i) {
	//		draw_aabb( uvmesh.group( i ).uv_boundingbox );
	//	}
	//	glEnd();

	//	glColor4f(1, 0, 1, 0.75);
	//	glBegin(GL_LINES);
	//	size_t nnum = uvmesh_normals.size() / 4;
	//	for (uint i = 0, uvi = 0; i < nnum; ++i, uvi += 4) {
	//		glVertex2f(uvmesh_normals[uvi] * tw, uvmesh_normals[uvi + 1] * th);
	//		glVertex2f(uvmesh_normals[uvi + 2] * tw, uvmesh_normals[uvi + 3] * th);
	//	}
	//	glEnd();

	draw_surface.end();

	ht.uv_position.set(
			(ofGetMouseX() - 10) * 1.f / draw_surface.getWidth(),
			(ofGetMouseY() - 10) * 1.f / draw_surface.getHeight()
			);

	uvmesh.inside_uv(ht);
	//std::cout << "HIT TEST COUNT " << ht.tests << std::endl;

	if (ht.success) {

		ht_center.set(0, 0, 0);
		int ht_center_div = 0;
		for (size_t f = 0; f < ht.faces.size(); ++f) {
			douv::InsideFace<ofVec3f, ofVec2f>& isf = ht.faces[f];
			if (isf.half_edge) {
				ht_center += ht.faces[f].face->xyz_center;
				ht_center_div++;
			}
		}
		if (ht_center_div > 0) {
			ht_center /= ht_center_div;
		}

		// display

		ofVec2f p;

		draw_surface.begin();

		ofFill();

		glBegin(GL_TRIANGLES);
		for (size_t f = 0; f < ht.faces.size(); ++f) {

			douv::InsideFace<ofVec3f, ofVec2f>& isf = ht.faces[f];
			const douv::Face<ofVec3f, ofVec2f>* face = isf.face;
			if (isf.half_edge) {
				glColor4f(0, 0, 0, 1);
			} else {
				glColor4f(0, 0, 0, 0.3);
			}
			for (uint8_t i = 0; i < DFACE_NUM; ++i) {
				p = face->vert(i)->uv * ts;
				glVertex2f(p.x, p.y);
			}

		}
		glEnd();

		glLineWidth(3.0f);
		glBegin(GL_LINES);
		for (size_t f = 0; f < ht.faces.size(); ++f) {

			douv::InsideFace<ofVec3f, ofVec2f>& isf = ht.faces[f];
			if (isf.half_edge) {

				glColor4f(1, 0, 0, 1);
				p = isf.half_edge->verts.first->uv * ts;
				glVertex2f(p.x, p.y);

				glColor4f(0, 1, 1, 1);
				p = isf.half_edge->verts.second->uv * ts;
				glVertex2f(p.x, p.y);

				glColor4f(1, 0, 1, 1);
				p = (isf.half_edge->verts.first->uv + isf.half_edge->uv_dir * isf.half_edge->uv_length * 0.5) * ts; // other->uv_normal * ts;
				glVertex2f(p.x, p.y);

				p += isf.half_edge->uv_normal * 20;
				glVertex2f(p.x, p.y);

				douv::Edge<ofVec3f, ofVec2f>* edge = isf.half_edge->edge;
				if (edge) {

					douv::HalfEdge<ofVec3f, ofVec2f>* other;
					other = edge->other(isf.half_edge);

					glColor4f(1, 0, 0, 1);
					p = other->verts.first->uv * ts;
					glVertex2f(p.x, p.y);

					glColor4f(0, 1, 1, 1);
					p = other->verts.second->uv * ts;
					glVertex2f(p.x, p.y);

					glColor4f(1, 0, 1, 1);
					p = (other->verts.first->uv + other->uv_dir * other->uv_length * 0.5) * ts; // other->uv_normal * ts;
					glVertex2f(p.x, p.y);

					p += other->uv_normal * 20;
					glVertex2f(p.x, p.y);

				}

			}

		}
		glEnd();
		glLineWidth(1.0f);

		glColor4f(1, 1, 1, 1);
		ofFill();
		
		bool projection_success = false;
		float cradius = 8;
		ofVec2f proj;

		ofVec2f dir(0, -25);
		ofVec2f dir1 = dir.getRotated(30);
		ofVec2f dir2 = dir.getRotated(-30);
		ofDrawLine(proj.x, proj.y, proj.x + dir.x, proj.y + dir.y);
		ofDrawLine(proj.x, proj.y, proj.x + dir1.x, proj.y + dir1.y);
		ofDrawLine(proj.x, proj.y, proj.x + dir2.x, proj.y + dir2.y);

		ofVec2f proj_dir;

		for (size_t f = 0; f < ht.faces.size(); ++f) {

			douv::InsideFace<ofVec3f, ofVec2f>& isf = ht.faces[f];
			if (isf.half_edge) {
				douv::Edge<ofVec3f, ofVec2f>* edge = isf.half_edge->edge;
				if (edge) {

					float sc = edge->uv_ratio(isf.half_edge);

					proj = ht.uv_position;
					bool projected = edge->uv_project_position(proj, isf.half_edge);
					if ( projected ) {
						projection_success = true;
					} else {
						continue;
					}
					
					proj *= surf_size;

					ofDrawCircle(proj, cradius * sc);

					proj_dir = dir;
					edge->uv_project_direction(proj_dir, isf.half_edge);
					ofDrawLine(proj.x, proj.y, proj.x + proj_dir.x * sc, proj.y + proj_dir.y * sc);
					proj_dir = dir1;
					edge->uv_project_direction(proj_dir, isf.half_edge);
					ofDrawLine(proj.x, proj.y, proj.x + proj_dir.x * sc, proj.y + proj_dir.y * sc);
					proj_dir = dir2;
					edge->uv_project_direction(proj_dir, isf.half_edge);
					ofDrawLine(proj.x, proj.y, proj.x + proj_dir.x * sc, proj.y + proj_dir.y * sc);


				}
			}

		}
		
		if (projection_success) {
			glColor4f(1, 1, 1, 1);
		} else {
			glColor4f(1, 0, 0, 1);
		}
		
		proj = ht.uv_position;
		proj *= surf_size;
		ofDrawCircle(proj, cradius);

		draw_surface.end();

	}

}

void douvApp::draw() {

	ofBackground( 20,20,20 );

	glColor4f(1, 1, 1, 0.4);

	ofDisableDepthTest();

	draw_surface.draw(10, 10);
	
	ofEnableDepthTest();

	glColor4f(1, 1, 1, 1);

	ofPushMatrix();

	ofTranslate(ofGetWidth() * 0.5, ofGetHeight() * 0.5, 0);
	ofRotateX(view_rotation.x);
	ofRotateY(view_rotation.y);
	ofRotateZ(view_rotation.z);

	float sc = ofGetHeight() * ( 1 / uvmesh.xyz_boundingbox.biggest() );
	glScalef(sc, sc, sc);

	draw_surface.getTexture().bind();

	glBegin(GL_TRIANGLES);

	int tw = draw_surface.getWidth();
	int th = draw_surface.getHeight();
	size_t fnum = uvmesh_xyzs.size() / 3;

	for (uint i = 0, xyzi = 0, uvi = 0; i < fnum; ++i, xyzi += 3, uvi += 2) {
		glTexCoord2f(uvmesh_uvs[uvi] * tw, uvmesh_uvs[uvi + 1] * th);
		glVertex3f(uvmesh_xyzs[xyzi], uvmesh_xyzs[xyzi + 1], uvmesh_xyzs[xyzi + 2]);
	}
	glEnd();

	draw_surface.getTexture().unbind();

	glColor4f(1, 1, 1, 0.3);
	glBegin(GL_LINES);
	draw_aabb(uvmesh.xyz_boundingbox);
	glEnd();


	if (ht.success) {
		ofFill();
		ofSetColor(0, 255, 0);
		//ofDrawSphere( ht_center, 0.1 );
	}

	ofPopMatrix();


}

void douvApp::mouseDragged(int x, int y, int button) {

	if (view_rotation_enabled) {

		ofVec2f diff(x, y);
		diff -= view_mouse_init;
		view_rotation = view_rotation_init + ofVec3f(-diff.y, 0, -diff.x);

	}

}

void douvApp::mousePressed(int x, int y, int button) {

	if (button == 2) {
		
		view_rotation_enabled = true;
		view_mouse_init.set(x, y);
		view_rotation_init = view_rotation;
		
	} else if ( button == 0 ) {
		
		travelers.bam( ofVec2f(
			(ofGetMouseX() - 10) * 1.f / draw_surface.getWidth(),
			(ofGetMouseY() - 10) * 1.f / draw_surface.getHeight()
			), 300 );
		
	} else if ( button == 1 ) {
		draw_surface.begin();
		ofClear(0, 0, 0, 255);
		draw_surface.end();
	}

}

void douvApp::mouseReleased(int x, int y, int button) {

	view_rotation_enabled = false;

}
