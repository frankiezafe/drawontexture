#include "intersect.h"

void intersect::setup() {

    ofBuffer buffer = ofBufferFromFile(DATA_FILE);
    if (buffer.size()) {

        std::vector< douv::Vertex<ofVec3f, ofVec2f> > tmp;

        for (ofBuffer::Line it = buffer.getLines().begin(), end = buffer.getLines().end(); it != end; ++it) {

            string line = *it;
            vector<string> coords = ofSplitString(line, ",");
            if (coords.size() == 5) {
                douv::Vertex<ofVec3f, ofVec2f> v(
                        atof(coords[0].c_str()),
                        atof(coords[1].c_str()),
                        atof(coords[2].c_str()),
                        atof(coords[3].c_str()) * 800,
                        atof(coords[4].c_str()) * 800
                        );
                tmp.push_back(v);
                if (tmp.size() == 3) {
                    uvmesh.push(tmp[0], tmp[1], tmp[2]);
                    tmp.clear();
                }
            }

        }

    }

    uvmesh.finalise();
    uvmesh.get_uv(uvmesh_uvs);

    dface.set(
            douv::Vertex<ofVec3f, ofVec2f>(0, 0, 0, 400, 370),
            douv::Vertex<ofVec3f, ofVec2f>(0, 0, 0, 430, 400),
            douv::Vertex<ofVec3f, ofVec2f>(0, 0, 0, 370, 400)
            );

}

void intersect::update() {
}

void intersect::draw() {

    ofClear(90, 90, 90, 255);

    ofEnableAlphaBlending();

    ofNoFill();
    glColor4f(0.2, 0.2, 0.2, 1);
    glBegin(GL_TRIANGLES);
    size_t fnum = uvmesh_uvs.size() / 2;
    for (uint i = 0, uvi = 0; i < fnum; ++i, uvi += 2) {
        glVertex2f(uvmesh_uvs[uvi], uvmesh_uvs[uvi + 1]);
    }
    glEnd();

    if (dface.dragged == -1) {

        int sel = -1;
        float mind = TOLERANCE;
        for (uint8_t i = 0; i < 3; ++i) {
            douv::Vertex<ofVec3f, ofVec2f>& v = dface[i];
            float d = sqrt(pow(ofGetMouseX() - v.uv.x, 2) + pow(ofGetMouseY() - v.uv.y, 2));
            if (d < mind) {
                mind = d;
                sel = i;
            }

        }

        if (sel != -1) {
            ofNoFill();
            ofSetColor(255, 0, 0);
            ofDrawCircle(dface[ sel ].uv.x, dface[ sel ].uv.y, TOLERANCE);
        }

    }

    ofFill();

    intersect_res.reset();
    intersect_res.face = &dface;

    auto intersect_begin = std::chrono::high_resolution_clock::now();
    uvmesh.intersect_uv(intersect_res);
    auto intersect_end = std::chrono::high_resolution_clock::now();

    while (intersect_avrg.size() > 100) {
        intersect_avrg.erase(intersect_avrg.begin());
    }
    intersect_avrg.push_back(std::chrono::duration_cast<std::chrono::nanoseconds>(intersect_end - intersect_begin).count());
    float avg = 0;
    for (size_t i = 0; i < intersect_avrg.size(); ++i) {
        avg += intersect_avrg[i] * 1.f / intersect_avrg.size();
    }

    std::stringstream ss;
    ss << int( avg) << " nanos\n";
    ss << (int( avg / 10000) * 0.01) << " millis";
    intersect_duration = ss.str();

    if (intersect_res.success) {

        std::vector< douv::IntersectionFace<ofVec3f, ofVec2f> >::iterator it;
        std::vector< douv::IntersectionFace<ofVec3f, ofVec2f> >::iterator ite;

        glBegin(GL_TRIANGLES);
        it = intersect_res.ifaces.begin();
        ite = intersect_res.ifaces.end();
        for (; it != ite; ++it) {
            douv::IntersectionFace<ofVec3f, ofVec2f>& ifa = (*it);
            if (ifa.is_inside) {
                glColor4f(0, 1, 1, 0.6);
            } else if (ifa.wraps) {
                glColor4f(1, 1, 0, 0.6);
            } else if (ifa.verts_inside == 1 && ifa.verts_wrapped == 0) {
                glColor4f(1, 0, 0, 0.3);
            } else if (ifa.verts_inside == 0 && ifa.verts_wrapped == 1) {
                glColor4f(0, 1, 0, 0.3);
            } else {
                glColor4f(1, 1, 1, 0.3);
            }
            for (uint i = 0; i < 3; ++i) {
                glVertex2f((*ifa.face)[i].uv.x, (*ifa.face)[i].uv.y);
            }

        }
        glEnd();

        glColor4f(0, 1, 0, 1);
        glPointSize(5);
        glBegin(GL_POINTS);
        it = intersect_res.ifaces.begin();
        for (; it != ite; ++it) {
            douv::IntersectionFace<ofVec3f, ofVec2f>& ifa = (*it);
            if (ifa.is_inside || ifa.wraps) {
                continue;
            }
            if (ifa.intersection_num > 5) {
                std::cout << int( ifa.intersection_num) << std::endl;
            }
            for (uint8_t i = 0; i < ifa.intersection_num; ++i) {
                glVertex2f(ifa.intersections[i].x, ifa.intersections[i].y);
            }
        }
        glEnd();
        glPointSize(1);

    }

    ofNoFill();
    glColor4f(1, 0, 0, 0.6);
    glBegin(GL_TRIANGLES);
    for (uint i = 0; i < 3; ++i) {
        glVertex2f(dface[i].uv.x, dface[i].uv.y);
    }
    glEnd();

    glColor4f(1, 1, 1, 0.6);
    glBegin(GL_LINES);
    glVertex2f(dface.uv_center.x - 5, dface.uv_center.y);
    glVertex2f(dface.uv_center.x + 5, dface.uv_center.y);
    glVertex2f(dface.uv_center.x, dface.uv_center.y - 5);
    glVertex2f(dface.uv_center.x, dface.uv_center.y + 5);
    glEnd();

    ofSetColor(255, 255, 255);
    ofDrawBitmapString(intersect_duration, 10, 25);

}

void intersect::mouseDragged(int x, int y, int button) {

    if (view_rotation_enabled) {

        ofVec2f diff(x, y);
        diff -= view_mouse_init;
        view_rotation = view_rotation_init + ofVec3f(-diff.y, 0, -diff.x);

    } else if (dface.dragged != -1) {

        dface[ dface.dragged ].uv.x = x - offset_mouse.x;
        dface[ dface.dragged ].uv.y = y - offset_mouse.y;
        dface.refresh();

    }

}

void intersect::mousePressed(int x, int y, int button) {

    if (button == 2) {

        view_rotation_enabled = true;
        view_mouse_init.set(x, y);
        view_rotation_init = view_rotation;

    } else if (button == 0) {

        dface.dragged = -1;
        float mind = TOLERANCE;

        for (uint i = 0; i < 3; ++i) {
            douv::Vertex<ofVec3f, ofVec2f>& v = dface[i];
            float d = sqrt(pow(x - v.uv.x, 2) + pow(y - v.uv.y, 2));
            if (d < mind) {
                mind = d;
                dface.dragged = i;
                offset_mouse.set(x - v.uv.x, y - v.uv.y);
            }
        }

    } else if (button == 1) {
    }

}

void intersect::mouseReleased(int x, int y, int button) {

    view_rotation_enabled = false;
    dface.dragged = -1;

}