public ArrayList< Tri > tris;

void setup() {
  
  size( 1024, 768 );
  tris = new ArrayList<Tri>();
  tris.add( new Tri( 50,50, 150,200, 80,190 ) );
  tris.add( new Tri( 50,50, 180,150, 150,200 ) );
  tris.add( new Tri( 180,150, 250,300, 150,200 ) );
  
}

void draw() {

  background(0);
  
  PVector mouse = new PVector( mouseX, mouseY );
  
  for ( Tri t : tris ) {
    t.hover( mouse );
    t.draw( mouse );
  }
  
}