public class Tri {

  public static final int EDGE_NUM = 3;
  public static final float HIGHLIGHT_MARGIN = 4;

  private boolean hit;
  private boolean hit_aabb;

  private ArrayList<PVector> vertices;

  private PVector aabb_min;
  private PVector aabb_max;
  private PVector aabb_center;

  private ArrayList<Float> lengths;
  private ArrayList<PVector> edge_dirs;
  private ArrayList<PVector> edge_mids;
  private ArrayList<PVector> edge_norms;
  private ArrayList<PVector> edge_distances;
  private PVector[] closest_edge;

  public Tri( 
    float x0, float y0, 
    float x1, float y1, 
    float x2, float y2
    ) 
  {

    hit = false;
    hit_aabb = false;

    vertices = new ArrayList<PVector>();
    vertices.add( new PVector( x0, y0 ) );
    vertices.add( new PVector( x1, y1 ) );
    vertices.add( new PVector( x2, y2 ) );

    lengths = new ArrayList<Float>();
    edge_dirs = new ArrayList<PVector>();
    edge_mids = new ArrayList<PVector>();
    edge_norms = new ArrayList<PVector>();
    edge_distances = new ArrayList<PVector>();

    process();
  }

  public void scale( float s ) {

    for ( PVector v : vertices ) {
      v.mult( s );
    }

    process();
    
  }

  private void process() {

    for ( int i = 0, j = 1; i < EDGE_NUM; ++i, ++j ) {
      j %= EDGE_NUM;
      PVector v1 = vertices.get(i);
      PVector v2 = vertices.get(j);

      if ( edge_norms.size() <= i ) {
        lengths.add( 0.f );
        edge_mids.add( new PVector() );
        edge_dirs.add( new PVector() );
        edge_norms.add( new PVector() );
        edge_distances.add( new PVector() );
      }

      if ( aabb_min == null || i == 0 ) {

        aabb_min = new PVector( v1.x, v1.y );
        aabb_max = new PVector( v1.x, v1.y );
        aabb_center = new PVector();
        
      } else {

        if ( aabb_min.x > v1.x ) { 
          aabb_min.x = v1.x;
        }
        if ( aabb_min.y > v1.y ) { 
          aabb_min.y = v1.y;
        }
        if ( aabb_max.x < v1.x ) { 
          aabb_max.x = v1.x;
        }
        if ( aabb_max.y < v1.y ) { 
          aabb_max.y = v1.y;
        }
      }

      PVector n = new PVector( v2.x - v1.x, v2.y - v1.y );
      edge_mids.get(i).set( new PVector( v1.x + n.x * 0.5, v1.y + n.y * 0.5 ) );
      lengths.set(i, n.mag());
      n.normalize();
      edge_dirs.get(i).set( new PVector( n.x, n.y ) );
      edge_norms.get(i).set( new PVector( n.y, -n.x ) );
    }

    aabb_center.set( aabb_min.x + ( aabb_max.x - aabb_min.x ) * 0.5, aabb_min.y + ( aabb_max.y - aabb_min.y ) * 0.5 );
  }

  public void hover( PVector m ) {

    hit = true;
    hit_aabb = true;

    if ( m.x < aabb_min.x || m.y < aabb_min.y || m.x > aabb_max.x || m.y > aabb_max.y ) {
      hit = false;
      hit_aabb = false;
      return;
    }

    for ( int i = 0; i < EDGE_NUM; ++i ) {

      PVector vertex = vertices.get(i);
      PVector norm = edge_norms.get(i);
      PVector dir = edge_dirs.get(i);

      PVector dist = new PVector( m.x - vertex.x, m.y - vertex.y );
      if ( dist.dot( norm ) > 0 ) {
        hit = false;
      }
    }

    if ( hit ) {

      float shortdest_dist = 0;

      for ( int i = 0, j = 1; i < EDGE_NUM; ++i, ++j ) {

        j %= EDGE_NUM;

        float l = lengths.get(i);
        PVector vertex = vertices.get(i);
        PVector dir = edge_dirs.get(i);
        //float d = rel.dot( dir ); // << ratio of dir
        float d = new PVector( (m.x - vertex.x) / l, (m.y - vertex.y) / l ).dot( dir ); // << ratio of dir
        PVector rel = new PVector( vertex.x + dir.x * d * l - m.x, vertex.y + dir.y * d * l - m.y );
        edge_distances.get( i ).set( rel.x, rel.y );
        d = rel.mag();

        if ( shortdest_dist > d || i == 0 ) {

          shortdest_dist = d;

          if ( closest_edge == null ) {
            closest_edge = new PVector[4];
            closest_edge[0] = new PVector();
            closest_edge[1] = new PVector();
            closest_edge[2] = new PVector();
            closest_edge[3] = new PVector();
          }

          closest_edge[0].set( vertices.get(i) );
          closest_edge[1].set( vertices.get(j) );
          closest_edge[2].set( dir );
          closest_edge[3].set( edge_norms.get(i) );
        }
      }
    } else {

      closest_edge = null;
    }
  }

  public void draw( PVector m ) {

    stroke( 255 );
    strokeWeight(1);
    if ( hit ) {
      fill( 255, 100 );
    } else {
      noFill();
    }

    beginShape();
    for ( PVector v : vertices ) {
      vertex(v.x, v.y);
    }
    endShape(CLOSE);

    noFill();
    if ( hit_aabb ) {
      stroke( 255, 100 );
    } else {
      stroke( 255, 20 );
    }
    beginShape();
    vertex( aabb_min.x, aabb_min.y );
    vertex( aabb_max.x, aabb_min.y );
    vertex( aabb_max.x, aabb_max.y );
    vertex( aabb_min.x, aabb_max.y );
    endShape(CLOSE);

    for ( int i = 0; i < EDGE_NUM; ++i ) {

      PVector vert = vertices.get(i);
      PVector mid = edge_mids.get(i);
      PVector dir = edge_dirs.get(i);
      PVector norm = edge_norms.get(i);

      strokeWeight(2);
      stroke( 255, 255, 0 );
      line( mid.x, mid.y, mid.x + dir.x * 20, mid.y + dir.y * 20 );
      stroke( 255, 0, 255 );
      line( mid.x, mid.y, mid.x + norm.x * 20, mid.y + norm.y * 20 );

      if ( hit ) { 
        PVector ed = edge_distances.get(i);

        noFill();
        strokeWeight(1);
        stroke( 0, 255, 255 );
        line( m.x, m.y, m.x + ed.x, m.y + ed.y );
        text( ed.mag(), m.x + ed.x, m.y + ed.y );

        beginShape();
        vertex( closest_edge[0].x - (closest_edge[2].x * HIGHLIGHT_MARGIN) - (closest_edge[3].x * HIGHLIGHT_MARGIN), closest_edge[0].y - (closest_edge[2].y * HIGHLIGHT_MARGIN) - (closest_edge[3].y * HIGHLIGHT_MARGIN) );
        vertex( closest_edge[0].x - (closest_edge[2].x * HIGHLIGHT_MARGIN) + (closest_edge[3].x * HIGHLIGHT_MARGIN), closest_edge[0].y - (closest_edge[2].y * HIGHLIGHT_MARGIN) + (closest_edge[3].y * HIGHLIGHT_MARGIN) );
        vertex( closest_edge[1].x + (closest_edge[2].x * HIGHLIGHT_MARGIN) + (closest_edge[3].x * HIGHLIGHT_MARGIN), closest_edge[1].y + (closest_edge[2].y * HIGHLIGHT_MARGIN) + (closest_edge[3].y * HIGHLIGHT_MARGIN) );
        vertex( closest_edge[1].x + (closest_edge[2].x * HIGHLIGHT_MARGIN) - (closest_edge[3].x * HIGHLIGHT_MARGIN), closest_edge[1].y + (closest_edge[2].y * HIGHLIGHT_MARGIN) - (closest_edge[3].y * HIGHLIGHT_MARGIN) );        
        endShape(CLOSE);
      }
    }
  }
}