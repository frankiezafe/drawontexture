public class Tri {

  public static final int EDGE_NUM = 3;
  public static final float HIGHLIGHT_MARGIN = 4;

  public static final int INTERSECT_NONE = 0;
  public static final int INTERSECT_ONE = 1;
  public static final int INTERSECT_TWO = 2;
  public static final int INTERSECT_THREE = 3;

  protected boolean hit;
  protected boolean hit_aabb;

  protected ArrayList<PVector> vertices;

  protected PVector aabb_min;
  protected PVector aabb_max;
  protected PVector aabb_size;
  protected PVector aabb_center;

  protected ArrayList<Float> lengths;
  protected ArrayList<PVector> edge_dirs;
  protected ArrayList<PVector> edge_mids;
  protected ArrayList<PVector> edge_norms;
  protected ArrayList<PVector> edge_distances;
  protected PVector[] closest_edge;
  
  public int rgb;

  public Tri( 
    float x0, float y0, 
    float x1, float y1, 
    float x2, float y2
    ) 
  {

    hit = false;
    hit_aabb = false;

    vertices = new ArrayList<PVector>();
    vertices.add( new PVector( x0, y0 ) );
    vertices.add( new PVector( x1, y1 ) );
    vertices.add( new PVector( x2, y2 ) );

    lengths = new ArrayList<Float>();
    edge_dirs = new ArrayList<PVector>();
    edge_mids = new ArrayList<PVector>();
    edge_norms = new ArrayList<PVector>();
    edge_distances = new ArrayList<PVector>();

    process();
    
  }
  
  public Tri( PVector v0, PVector v1, PVector v2) 
  {

    hit = false;
    hit_aabb = false;

    vertices = new ArrayList<PVector>();
    vertices.add( new PVector( v0.x, v0.y ) );
    vertices.add( new PVector( v1.x, v1.y ) );
    vertices.add( new PVector( v2.x, v2.y ) );

    lengths = new ArrayList<Float>();
    edge_dirs = new ArrayList<PVector>();
    edge_mids = new ArrayList<PVector>();
    edge_norms = new ArrayList<PVector>();
    edge_distances = new ArrayList<PVector>();

    process();
  }

  public void scale( float s ) {

    for ( PVector v : vertices ) {
      v.mult( s );
    }
    process();
  }

  protected void process() {

    rgb = color( random(127,255), random(30,120), random(0,50) );

    for ( int i = 0, j = 1; i < EDGE_NUM; ++i, ++j ) {
      j %= EDGE_NUM;
      PVector v1 = vertices.get(i);
      PVector v2 = vertices.get(j);

      if ( edge_norms.size() <= i ) {
        lengths.add( 0.f );
        edge_mids.add( new PVector() );
        edge_dirs.add( new PVector() );
        edge_norms.add( new PVector() );
        edge_distances.add( new PVector() );
      }

      if ( aabb_min == null || i == 0 ) {

        aabb_min = new PVector( v1.x, v1.y );
        aabb_max = new PVector( v1.x, v1.y );
        aabb_center = new PVector();
        aabb_size = new PVector();
      } else {

        if ( aabb_min.x > v1.x ) { 
          aabb_min.x = v1.x;
        }
        if ( aabb_min.y > v1.y ) { 
          aabb_min.y = v1.y;
        }
        if ( aabb_max.x < v1.x ) { 
          aabb_max.x = v1.x;
        }
        if ( aabb_max.y < v1.y ) { 
          aabb_max.y = v1.y;
        }
      }

      PVector n = new PVector( v2.x - v1.x, v2.y - v1.y );
      edge_mids.get(i).set( new PVector( v1.x + n.x * 0.5, v1.y + n.y * 0.5 ) );
      lengths.set(i, n.mag());
      n.normalize();
      edge_dirs.get(i).set( new PVector( n.x, n.y ) );
      edge_norms.get(i).set( new PVector( n.y, -n.x ) );
    }

    aabb_center.set( aabb_min.x + ( aabb_max.x - aabb_min.x ) * 0.5, aabb_min.y + ( aabb_max.y - aabb_min.y ) * 0.5 );
    aabb_size.set( aabb_max.x - aabb_min.x, aabb_max.y - aabb_min.y );
  }

  public int inside( Tri other ) {

    int out = 0;
    for ( PVector v : vertices ) {
      if ( other.inside( v ) ) {
        out++;
      }
    }
    return out;
  }

  public boolean inside( PVector pos ) {

    boolean out = true;
    if ( 
      pos.x < aabb_min.x || pos.y < aabb_min.y || 
      pos.x > aabb_max.x || pos.y > aabb_max.y ) {
      hit = false;
      hit_aabb = false;
      return false;
    }
    for ( int i = 0; i < EDGE_NUM; ++i ) {
      PVector vertex = vertices.get(i);
      PVector norm = edge_norms.get(i);
      PVector dist = new PVector( pos.x - vertex.x, pos.y - vertex.y );
      if ( dist.dot( norm ) > 0 ) {
        out = false;
      }
    }
    return out;
  }

  public PVector intersect( PVector a0, PVector a1, PVector b0, PVector b1 ) {
    float a = a1.x - a0.x;
    float b = b0.x - b1.x;
    float c = a1.y - a0.y;
    float d = b0.y - b1.y;
    float e = b0.x - a0.x;
    float f = b0.y - a0.y;
    float denom = a * d - b * c;
    if ( abs( denom ) < 1e-5 ) {
      // parrallel
      return null;
    } else {
      float t = (e*d - b*f)/denom;
      float u = (a*f - e*c)/denom;
      if (
        ( t >= 0 && t <= 1 ) && 
        ( u >= 0 && u <= 1 )
        ) {
        return new PVector( 
          a0.x + t * ( a1.x - a0.x ), 
          a0.y + t * ( a1.y - a0.y ) );
      }
      return null;
    }
  }

  public ArrayList<PVector> intersect( PVector a0, PVector a1 ) {
    ArrayList<PVector> tmp = new ArrayList<PVector>();
    for ( int i = 0, j = 1; i < EDGE_NUM; ++i, ++j ) {
      j %= EDGE_NUM;
      PVector v0 = vertices.get(i);
      PVector v1 = vertices.get(j);
      PVector res = intersect( v0, v1, a0, a1 );
      if ( res != null ) {
        tmp.add( res );
      }
    }
    // sort results
    ArrayList<PVector> out = new ArrayList<PVector>();
    while( tmp.size() > 0 ) {
      if ( tmp.size() == 1 ) {
        out.addAll( tmp );
        tmp.clear();
      } else {
        PVector closest = null;
        float smallest = 0;
        for ( PVector v : tmp ) {
          if ( closest == null ) {
            closest = v;
            smallest = v.dist( a0 );
          } else {
            float d = v.dist( a0 );
            if ( smallest > d ) {
              closest = v;
              smallest = d;
            }
          }
        }
        if ( closest == null ) {
          break;
        }
        tmp.remove( closest );
        out.add( closest );
      }
    }
    return out;
  }

  public ArrayList<PVector> intersect( Tri other ) {
    ArrayList<PVector> out = new ArrayList<PVector>();
    for ( int i = 0, j = 1; i < EDGE_NUM; ++i, ++j ) {
      j %= EDGE_NUM;
      PVector v0 = vertices.get(i);
      PVector v1 = vertices.get(j);
      for ( int k = 0, l = 1; k < EDGE_NUM; ++k, ++l ) {
        l %= EDGE_NUM;
        PVector v2 = other.vertices.get(k);
        PVector v3 = other.vertices.get(l);
        PVector res = intersect( v0, v1, v2, v3 );
        if ( res != null ) {
          out.add( res );
        }
      }
    }
    return out;
  }

  public ArrayList< ArrayList< PVector > > turncate( Tri other ) {
    ArrayList< PVector > tmp = new ArrayList< PVector >();
    for ( int h = 2, i = 0, j = 1; i < vertices.size(); ++i, ++j ) {
      h %= EDGE_NUM;
      j %= EDGE_NUM;
      PVector v0 = vertices.get(i);
      if ( other.inside( v0 ) ) {
        tmp.add( v0 );
      }
      ArrayList<PVector> inters;
      //inters = other.intersect( vertices.get(h), v0 );
      //tmp.addAll( inters );
      inters = other.intersect( v0, vertices.get(j) );
      tmp.addAll( inters );
    }
    ArrayList< ArrayList< PVector > > out = new ArrayList< ArrayList< PVector > >();
    for ( int i = 2; i < tmp.size(); ++i ) {
      ArrayList< PVector > tmp2 = new ArrayList< PVector >();
      tmp2.add( tmp.get(i-2) );
      tmp2.add( tmp.get(i-1) );
      tmp2.add( tmp.get(i) );
      out.add( tmp2 );
    }
    return out;
  }

  public void hover( PVector m ) {

    hit = true;
    hit_aabb = true;

    if ( m.x < aabb_min.x || m.y < aabb_min.y || m.x > aabb_max.x || m.y > aabb_max.y ) {
      hit = false;
      hit_aabb = false;
      return;
    }

    for ( int i = 0; i < EDGE_NUM; ++i ) {
      PVector vertex = vertices.get(i);
      PVector norm = edge_norms.get(i);
      PVector dir = edge_dirs.get(i);
      PVector dist = new PVector( m.x - vertex.x, m.y - vertex.y );
      if ( dist.dot( norm ) > 0 ) {
        hit = false;
      }
    }

    if ( hit ) {

      float shortdest_dist = 0;

      for ( int i = 0, j = 1; i < EDGE_NUM; ++i, ++j ) {

        j %= EDGE_NUM;

        float l = lengths.get(i);
        PVector vertex = vertices.get(i);
        PVector dir = edge_dirs.get(i);
        //float d = rel.dot( dir ); // << ratio of dir
        float d = new PVector( (m.x - vertex.x) / l, (m.y - vertex.y) / l ).dot( dir ); // << ratio of dir
        PVector rel = new PVector( vertex.x + dir.x * d * l - m.x, vertex.y + dir.y * d * l - m.y );
        edge_distances.get( i ).set( rel.x, rel.y );
        d = rel.mag();

        if ( shortdest_dist > d || i == 0 ) {

          shortdest_dist = d;

          if ( closest_edge == null ) {
            closest_edge = new PVector[4];
            closest_edge[0] = new PVector();
            closest_edge[1] = new PVector();
            closest_edge[2] = new PVector();
            closest_edge[3] = new PVector();
          }

          closest_edge[0].set( vertices.get(i) );
          closest_edge[1].set( vertices.get(j) );
          closest_edge[2].set( dir );
          closest_edge[3].set( edge_norms.get(i) );
        }
      }
    } else {

      closest_edge = null;
    }
  }

  public void draw( PVector m ) {

    fill( rgb );
    if ( hit ) {
      stroke( 255, 100 );
      strokeWeight(1);
    } else {
      noStroke();
    }
    
    beginShape();
    for ( PVector v : vertices ) {
      vertex(v.x, v.y);
    }
    endShape(CLOSE);

    /*
    noFill();
     if ( hit_aabb ) {
     stroke( 255, 100 );
     } else {
     stroke( 255, 20 );
     }
     beginShape();
     vertex( aabb_min.x, aabb_min.y );
     vertex( aabb_max.x, aabb_min.y );
     vertex( aabb_max.x, aabb_max.y );
     vertex( aabb_min.x, aabb_max.y );
     endShape(CLOSE);
     
     for ( int i = 0; i < EDGE_NUM; ++i ) {
     
     PVector vert = vertices.get(i);
     PVector mid = edge_mids.get(i);
     PVector dir = edge_dirs.get(i);
     PVector norm = edge_norms.get(i);
     
     strokeWeight(2);
     stroke( 255, 255, 0 );
     line( mid.x, mid.y, mid.x + dir.x * 20, mid.y + dir.y * 20 );
     stroke( 255, 0, 255 );
     line( mid.x, mid.y, mid.x + norm.x * 20, mid.y + norm.y * 20 );
     
     if ( hit ) { 
     PVector ed = edge_distances.get(i);
     
     noFill();
     strokeWeight(1);
     stroke( 0, 255, 255 );
     line( m.x, m.y, m.x + ed.x, m.y + ed.y );
     text( ed.mag(), m.x + ed.x, m.y + ed.y );
     
     beginShape();
     vertex( closest_edge[0].x - (closest_edge[2].x * HIGHLIGHT_MARGIN) - (closest_edge[3].x * HIGHLIGHT_MARGIN), closest_edge[0].y - (closest_edge[2].y * HIGHLIGHT_MARGIN) - (closest_edge[3].y * HIGHLIGHT_MARGIN) );
     vertex( closest_edge[0].x - (closest_edge[2].x * HIGHLIGHT_MARGIN) + (closest_edge[3].x * HIGHLIGHT_MARGIN), closest_edge[0].y - (closest_edge[2].y * HIGHLIGHT_MARGIN) + (closest_edge[3].y * HIGHLIGHT_MARGIN) );
     vertex( closest_edge[1].x + (closest_edge[2].x * HIGHLIGHT_MARGIN) + (closest_edge[3].x * HIGHLIGHT_MARGIN), closest_edge[1].y + (closest_edge[2].y * HIGHLIGHT_MARGIN) + (closest_edge[3].y * HIGHLIGHT_MARGIN) );
     vertex( closest_edge[1].x + (closest_edge[2].x * HIGHLIGHT_MARGIN) - (closest_edge[3].x * HIGHLIGHT_MARGIN), closest_edge[1].y + (closest_edge[2].y * HIGHLIGHT_MARGIN) - (closest_edge[3].y * HIGHLIGHT_MARGIN) );        
     endShape(CLOSE);
     }
     }
     */
  }
}