public class TriDraggable extends Tri {
  
  private ArrayList<PVector> cross_pts;
  private PImage uv_tex;
  
  public TriDraggable( 
    float x0, float y0, 
    float x1, float y1, 
    float x2, float y2
    ) 
  {
    super( x0,y0, x1,y1, x2,y2 );
    cross_pts = new ArrayList<PVector>();
    uv_tex = loadImage( "uv_grid.png" );
  }
  
  public void move( int id, float x, float y ) {
    vertices.get(id).set( x,y );
    process();
  }
  
  public void intersect( ArrayList< Tri > tris ) {
    
    cross_pts.clear();
    
    ArrayList< Tri > tri_fully_inside = new ArrayList< Tri >();
    ArrayList< Tri > tri_partial_inside = new ArrayList< Tri >();
    ArrayList< Tri > tri_fully_include = new ArrayList< Tri >();
    ArrayList< Tri > tri_partial_include = new ArrayList< Tri >();
    ArrayList< Tri > tri_intersect = new ArrayList< Tri >();
    ArrayList< Tri > tri_not_inside0 = new ArrayList< Tri >();
    ArrayList< Tri > tri_not_inside1 = new ArrayList< Tri >();
    
    for ( Tri t : tris ) {
      int pt_num = t.inside( this );
      if ( pt_num == Tri.INTERSECT_THREE ) {
        tri_fully_inside.add( t );
      } else if ( pt_num != Tri.INTERSECT_NONE ) {
        pt_num = this.inside( t );
        if ( 
          pt_num != Tri.INTERSECT_THREE &&
          pt_num != Tri.INTERSECT_NONE ) {
          tri_partial_include.add( t );
        } else {
          tri_partial_inside.add( t );
        }
      } else {
        tri_not_inside0.add( t );
      }
    }
    
    for ( Tri t : tri_not_inside0 ) {
      int pt_num = this.inside( t );
      if ( pt_num == Tri.INTERSECT_THREE ) {
        tri_fully_include.add( t );
      } else if ( pt_num != Tri.INTERSECT_NONE ) {
        tri_partial_include.add( t );
      } else {
        tri_not_inside1.add( t );
      }
    }
    
    for ( Tri t : tri_partial_inside ) {
      ArrayList<PVector> res = t.intersect( this );
      if ( res.size() != 0 ) {
        cross_pts.addAll( res );
      }
    }
    
    for ( Tri t : tri_not_inside1 ) {
      ArrayList<PVector> res = t.intersect( this );
      if ( res.size() != 0 ) {
        cross_pts.addAll( res );
        if ( !tri_intersect.contains( t ) ) {
          tri_intersect.add( t );
        }
      }
    }
    
    noStroke();
    /*
    fill( 255 );
    for ( Tri t : tri_fully_inside ) {
      beginShape();
      texture(uv_tex);
      for ( PVector v : t.vertices ) {
        vertex(v.x, v.y, 0, v.x / width, v.y / width);
      }
      endShape(CLOSE);
    }
    fill( 0,255,255, 200 );
    for ( Tri t : tri_partial_inside ) {
      beginShape();
      for ( PVector v : t.vertices ) {
        vertex(v.x, v.y);
      }
      endShape(CLOSE);
    }
    fill( 255,0,255, 200 );
    for ( Tri t : tri_partial_include ) {
      beginShape();
      for ( PVector v : t.vertices ) {
        vertex(v.x, v.y);
      }
      endShape(CLOSE);
    }
    */
    fill( 255,255,0, 200 );
    for ( Tri t : tri_intersect ) {
      beginShape();
      for ( PVector v : t.vertices ) {
        vertex(v.x, v.y);
      }
      endShape(CLOSE);
    }
    
    // final rendering
    stroke( 255 );
    strokeWeight( 1 );
    fill( 255, 200 );
    for ( Tri t : tri_fully_inside ) {
      beginShape();
      texture(uv_tex);
      for ( PVector v : t.vertices ) {
        vertex(v.x, v.y, 0, v.x / width, v.y / width);
      }
      endShape(CLOSE);
    }
    for ( Tri t : tri_partial_inside ) {
      beginShape();
      texture(uv_tex);
      ArrayList< ArrayList< PVector > > trivecs = t.turncate( this );
      for ( ArrayList< PVector > vs : trivecs ) {
      for ( PVector v : vs ) {
        vertex(v.x, v.y, 0, v.x / width, v.y / width);
      }
      }
      endShape(CLOSE);
    }
    
    for ( Tri t : tri_partial_include ) {
      beginShape();
      texture(uv_tex);
      ArrayList< ArrayList< PVector > > trivecs = this.turncate( t );
      for ( ArrayList< PVector > vs : trivecs ) {
      for ( PVector v : vs ) {
        vertex(v.x, v.y, 0, v.x / width, v.y / width);
      }
      }
      endShape(CLOSE);
    }
    
    strokeWeight( 1 );
    
    /*
    for ( PVector v : vertices ) {
      for ( Tri t : tris ) {
        boolean success = false;
        if ( t.inside( v ) ) {
          if ( !pthit.contains( t ) ) {
            pthit.add( t );
            success = true;
          }
        } else {
          for ( PVector tv : t.vertices ) {
            if ( inside( tv ) ) {
              if ( !pthit.contains( t ) ) {
                pthit.add( t );
                success = true;
                break;
              }
            }
          }
        }
        if ( !success ) {
          pthit_invalid.add( t );
        }
      }
    }
    
    for ( Tri t : pthit_invalid ) {
      ArrayList<PVector> res = t.intersect( this );
      if ( res.size() != 0 ) {
        cross_pts.addAll( res );
        if ( !pthit.contains( t ) ) {
          pthit.add( t );
        }
      }
    }
    
    noStroke();
    fill( 255, 50 );
    for ( Tri t : pthit ) {
      beginShape();
      for ( PVector v : t.vertices ) {
        vertex(v.x, v.y);
      }
      endShape(CLOSE);
    }
    fill( 255 );
    text( "touched: " + pthit.size() + "\navoided: " + pthit_invalid.size(), 10, 25 );
    */
    
  }
  
  public int hit( float x, float y ) {
    int id = 0;
    for ( PVector v : vertices ) {
      if ( dist( x, y, v.x, v.y ) < 10 ) {
        return id;
      }
      id++;
    }
    return -1;
  }
  
  public void draw( PVector m ) {   
    //super.draw( m );
    
    stroke( 255 );
    strokeWeight( 2 );
    noFill();
    beginShape(); 
    for ( PVector v : vertices ) {
      vertex(v.x, v.y, 0, v.x / width, v.y / width);
    }
    endShape(CLOSE);
    
    noStroke();
    fill( 255,0,0 );
    for ( PVector vert : vertices ) {
      ellipse( vert.x, vert.y, 10, 10 );
    }
    fill( 0,255,0 );
    for ( PVector vert : cross_pts ) {
      ellipse( vert.x, vert.y, 6, 6 );
    }
  }
  
}