public ArrayList< Tri > tris;
public TriDraggable trid;
public int dragged_pt;

void setup() {

  size( 1000, 700, P3D );
  
  int gap = 100;
  PVector[][] grid = new PVector[ int(height / gap) + 1 ][ int(width / gap) + 1 ];
  for ( int y = 0, yid = 0; y <= height; y += gap, ++yid ) {
    for ( int x = 0, xid = 0; x <= width; x += gap, ++xid ) {
      float yp = y;
      if ( y > 0 && y < height ) {
        yp += random( -gap*0.25, gap*0.25 );
      }
      float xp = x;
      if ( x > 0 && x < width ) {
        xp += random( -gap*0.25, gap*0.25 );
      }
      grid[yid][xid] = new PVector( xp, yp );
    }
  }

  tris = new ArrayList<Tri>();
  for ( int y = 0; y < grid.length - 1; ++y ) {
  for ( int x = 0; x < grid[y].length - 1; ++x ) {
    tris.add( new Tri( grid[y][x], grid[y][x+1], grid[y+1][x+1] ) );
    tris.add( new Tri( grid[y+1][x+1], grid[y+1][x], grid[y][x] ) );
  }  
  }
  // covering the screen with triangles
  //for ( int y = 0; y < height - 99; y += 100 ) {
  //  for ( int x = 0; x < width - 99; x += 100 ) {
  //    tris.add( new Tri( x, y, x+100, y, x+100, y+100 ) );
  //    tris.add( new Tri( x+100, y+100, x, y+100, x, y ) );
  //  }
  //}

  trid = new TriDraggable( 
    657, 223, 
    width*0.5 + 100, height*0.5 + 100, 
    width*0.5 - 100, height*0.5 + 100
    );
  dragged_pt = -1;

  textureMode(NORMAL);
}

void draw() {

  background(0);

  PVector mouse = new PVector( mouseX, mouseY );

  for ( Tri t : tris ) {
    //t.hover( mouse );
    t.draw( mouse );
  }
  
  trid.draw( mouse );
  trid.intersect( tris );

}

void mouseDragged() {

  if ( dragged_pt > -1 ) {
    trid.move( dragged_pt, mouseX, mouseY );
    println( mouseX + "," + mouseY );
  }
}

void mousePressed() {
  dragged_pt = trid.hit( mouseX, mouseY );
}

void mouseReleased() {
  dragged_pt =-1;
}