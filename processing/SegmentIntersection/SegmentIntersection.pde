class Segment {
  
  public PVector v0;
  public PVector v1;
  public PVector dir;
  public PVector norm;
  public PVector mid;
  
  public Segment( float x0, float y0, float x1, float y1 ) {
    v0 = new PVector( x0, y0 );
    v1 = new PVector( x1, y1 );
    process();
  }
  
  public void process() {
    mid = new PVector( 
      v0.x + ( v1.x - v0.x ) * 0.5, 
      v0.y + ( v1.y - v0.y ) * 0.5);
    dir = new PVector( v1.x - v0.x, v1.y - v0.y);
    dir.normalize();
    norm = new PVector( dir.y, -dir.x );
  }
  
  public void draw() {
    stroke( 255 );
    line( v0.x, v0.y, v1.x, v1.y );
    stroke( 255, 0, 255 );
    line( mid.x, mid.y, mid.x + norm.x * 20, mid.y + norm.y * 20 );
  }
  
  public PVector intersect( Segment s, boolean restricta, boolean restrictb ) {
    float a = v1.x - v0.x;
    float b = s.v0.x - s.v1.x;
    float c = v1.y - v0.y;
    float d = s.v0.y - s.v1.y;
    float e = s.v0.x - v0.x;
    float f = s.v0.y - v0.y;
    float denom = a * d - b * c;
    if ( abs( denom ) < 1e-5 ) {
      // parrallel
      return null;
    } else {
      float t = (e*d - b*f)/denom;
      float u = (a*f - e*c)/denom;
      if (
        ( !restricta || t >= 0 && t <= 1 ) && 
        ( !restrictb || u >= 0 && u <= 1 )
        ) {
        return new PVector( 
          v0.x + t * ( v1.x - v0.x ), 
          v0.y + t * ( v1.y - v0.y ) );
      }
      return null;
    }
  }
  
}

Segment s0;
Segment s1;
PVector dragged;

void setup() {
  size( 1000, 700 );
  s0 = new Segment( 100, 100, 900, 600 );
  s1 = new Segment( 950, 100, 200, 600 );
  dragged = null;
}

void draw() {
  background( 0 );
  s0.draw();
  s1.draw();
  PVector v = s0.intersect( s1, true, true );
  if ( v != null ) {
    noStroke();
    fill( 0, 255, 0 );
    ellipse( v.x, v.y, 10, 10 );
  }
}

void mouseDragged() {
  if ( dragged != null ) {
    dragged.set( mouseX, mouseY );
    s0.process();
  }
}

void mousePressed() {
  if ( dist( mouseX, mouseY, s0.v0.x, s0.v0.y ) < 10 ) {
    dragged = s0.v0;
  } else if ( dist( mouseX, mouseY, s0.v1.x, s0.v1.y ) < 10 ) {
    dragged = s0.v1;
  }
}

void mouseReleased() {
  dragged = null;
}