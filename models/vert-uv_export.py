import bpy, os

name = 'cylinder'

obj = bpy.data.objects[name]
mesh = obj.data
uv_layer = mesh.uv_layers["UVMap"].data

f = open( bpy.path.abspath('//' + name + '-vert-uv.txt'), 'w' )

for p in mesh.polygons:
    l = len( p.vertices )
    if l != 3:
        print( 'ERROR: TRIANGULATE YOUR MODEL FIRST!' )
        break
    for i in range( 0, l ):
        vert = mesh.vertices[ p.vertices[i] ]
        uv = uv_layer[ p.loop_start + i ]
        f.write( str( vert.co[0] ) + ',' )
        f.write( str( vert.co[1] ) + ',' )
        f.write( str( vert.co[2] ) + ',' )
        f.write( str( uv.uv[0] ) + ',' )
        f.write( str( uv.uv[1] ) + '\n' )
        f.flush()
        #print( vert.co, uv.uv )

f.close()